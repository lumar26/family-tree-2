package validation.impl.value;

import validation.Validator;

public abstract class ValueValidator implements Validator {
    private Validator next;

    @Override
    public Validator setNext(Validator next) {
        this.next = next;
        return this.next;
    }

    @Override
    public void validate() throws Exception {
        if (next != null)
            next.validate();
    }
}
