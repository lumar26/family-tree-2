package validation.impl.value.concrete;

import validation.Validator;
import validation.impl.value.ValueValidator;

public class AllAlphabetValidator extends ValueValidator {

    private final String string;

    public AllAlphabetValidator(String string) {
        this.string = string;
    }

    @Override
    public void validate() throws Exception {
        Validator base = new NotNullObjectValidator(string);
        base.setNext(new NonEmptyStringValidator(string));
        base.validate();
        if (!string.matches("^[a-zA-Z\u0100-\u017F\u2DE0-\u2DFF ]*$"))
            throw new Exception("String must be all alphabetic characters!");
        super.validate();
    }
}
