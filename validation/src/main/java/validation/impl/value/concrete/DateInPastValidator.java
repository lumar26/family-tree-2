package validation.impl.value.concrete;

import validation.impl.value.ValueValidator;

import java.time.LocalDate;

public class DateInPastValidator extends ValueValidator {

    private final LocalDate date;

    public DateInPastValidator(LocalDate date) {
        this.date = date;
    }

    @Override
    public void validate() throws Exception {
        if (date != null && date.isAfter(LocalDate.now()))
            throw new Exception("Datum mora biti u prošlosti!");
        super.validate();
    }
}
