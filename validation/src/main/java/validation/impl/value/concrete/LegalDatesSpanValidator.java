package validation.impl.value.concrete;

import validation.impl.value.ValueValidator;

import java.time.LocalDate;

public class LegalDatesSpanValidator extends ValueValidator {
    private final LocalDate birthDate;
    private final LocalDate deathDate;

    public LegalDatesSpanValidator(LocalDate birthDate, LocalDate deathDate) {
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }

    @Override
    public void validate() throws Exception {
        if (deathDate != null && deathDate.isBefore(birthDate))
            throw new Exception("Death date must not be before birth date!");
        if (deathDate != null && deathDate.minusYears(120).isAfter(birthDate))
            throw new Exception("Person cannot live mora than 120 years");
        super.validate();
    }
}
