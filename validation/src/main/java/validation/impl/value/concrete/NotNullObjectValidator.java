package validation.impl.value.concrete;

import validation.impl.value.ValueValidator;

public class NotNullObjectValidator extends ValueValidator {

    private final Object nullable;

    public NotNullObjectValidator(Object nullable) {
        this.nullable = nullable;
    }

    @Override
    public void validate() throws Exception {
        if (nullable == null)
            throw new Exception("Object must not be null!");
        super.validate();
    }
}
