package validation.impl.value.concrete;

import validation.impl.value.ValueValidator;

import java.time.LocalDate;

public class ChronologicalOrderValidator extends ValueValidator {
    private final LocalDate first;
    private final LocalDate second;


    public ChronologicalOrderValidator(LocalDate first, LocalDate second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public void validate() throws Exception {

        if (first != null && second != null && first.isAfter(second))
            throw new Exception("Wrong chronological order: " + first + " not before " + second);
        super.validate();
    }
}
