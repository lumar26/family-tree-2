package validation.impl.value.concrete;

import validation.impl.value.ValueValidator;

public class NonEmptyStringValidator extends ValueValidator {

    private String string;

    public NonEmptyStringValidator(String string) {
        this.string = string;
    }

    @Override
    public void validate() throws Exception {
        if (string == null) throw new Exception("String (tekst) mora da postoji!");
        if (string.isEmpty() || string.isBlank()) throw new Exception("String (tekst) new sme biti prazan!");
        super.validate();
    }
}
