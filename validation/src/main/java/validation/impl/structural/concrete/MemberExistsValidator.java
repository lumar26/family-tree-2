package validation.impl.structural.concrete;

import model.impl.Member;
import repository.Repository;
import validation.impl.structural.StructuralValidator;

public class MemberExistsValidator extends StructuralValidator {

    private final Member member;

    public MemberExistsValidator(Repository repository, Member member) {
        super(repository);
        this.member = member;
    }

    @Override
    public void validate() throws Exception {
        Member found = (Member) repository.findById(member.getId(), Member.class);
        if (member != null && found == null)
            throw new Exception("Member does not exist in database");
        super.validate();
    }
}
