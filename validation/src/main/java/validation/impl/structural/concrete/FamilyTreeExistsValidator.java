package validation.impl.structural.concrete;

import model.impl.FamilyTree;
import repository.Repository;
import validation.impl.structural.StructuralValidator;


public class FamilyTreeExistsValidator extends StructuralValidator {

    private final long treeID;

    public FamilyTreeExistsValidator(Repository repository, long treeID) {
        super(repository);
        this.treeID = treeID;
    }

    @Override
    public void validate() throws Exception {
        /*provera strukturnog ograničenja INSERT RESTRICTED User*/
        FamilyTree found = (FamilyTree) repository.findById(treeID, FamilyTree.class);
        if (treeID != 0 && found == null) throw new Exception("Rodoslov ne postoji u bazi!");
        super.validate();
    }
}
