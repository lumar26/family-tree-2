package validation.impl.structural.concrete;

import model.impl.Member;
import model.impl.Node;
import model.enums.Role;
import validation.Validator;
import validation.impl.structural.StructuralValidator;
import validation.impl.value.concrete.ChronologicalOrderValidator;

public class ParentChildValidator extends StructuralValidator {

    private final Member newMember;
    private final Node familyMemberNode;
    private final Role familyMemberRole;

    public ParentChildValidator(Member newMember, Node familyMemberNode, Role familyMemberRole) {
        super(null);
        this.newMember = newMember;
        this.familyMemberNode = familyMemberNode;
        this.familyMemberRole = familyMemberRole;
    }

    @Override
    public void validate() throws Exception {
        Validator parentBeforeChild = null;
        Validator childBeforeDeath = null;
        if (familyMemberNode != null && familyMemberRole.equals(Role.CHILD)) {
            parentBeforeChild = new ChronologicalOrderValidator(newMember.getBirthDate(), familyMemberNode.getMember().getBirthDate());
            if (newMember.getDeathDate() != null)
                childBeforeDeath = new ChronologicalOrderValidator(familyMemberNode.getMember().getBirthDate(), newMember.getDeathDate());
            parentBeforeChild.setNext(childBeforeDeath);
            parentBeforeChild.validate();

        }
        if (familyMemberNode != null && familyMemberRole.equals(Role.PARENT)) {
            parentBeforeChild = new ChronologicalOrderValidator(familyMemberNode.getMember().getBirthDate(), newMember.getBirthDate());
            if (newMember.getDeathDate() != null)
                childBeforeDeath = new ChronologicalOrderValidator(newMember.getBirthDate(), familyMemberNode.getMember().getDeathDate());
            parentBeforeChild.setNext(childBeforeDeath);
            parentBeforeChild.validate();
        }

        super.validate();
    }
}
