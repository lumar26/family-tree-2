package validation.impl.structural.concrete;

import model.impl.User;
import repository.Repository;
import validation.impl.structural.StructuralValidator;

public class UserExistsValidator extends StructuralValidator {

    private final long userID;

    public UserExistsValidator(Repository repository, long id) {
        super(repository);
        this.userID = id;
    }

    @Override
    public void validate() throws Exception {
        /*provera strukturnog ograničenja INSERT RESTRICTED User*/
        User found = (User) repository.findById(userID, User.class);
        if (userID != 0 && found == null) throw new Exception("Korisnik ne postoji u bazi!");
        super.validate();
    }
}
