package validation.impl.structural.concrete;

import model.impl.Node;
import model.enums.Role;
import repository.Repository;
import validation.impl.structural.StructuralValidator;

import java.sql.SQLException;

public class SingleParentValidator extends StructuralValidator {

    private final Node familyMemberNode;
    private final Role familyMemberRole;

    public SingleParentValidator(Repository repository, long familyMemberID, Role familyMemberRole) throws SQLException {
        super(repository);
        this.familyMemberNode = (Node) repository.findById(familyMemberID, Node.class);
        this.familyMemberRole = familyMemberRole;
    }

    @Override
    public void validate() throws Exception {
        /*ako je clan porodice dete ne sme vec da ima oca, nego ovaj clan mora da mu bude otac*/
        if (familyMemberNode != null && familyMemberNode.hasParent() && familyMemberRole.equals(Role.CHILD))
            throw new Exception("Novi član ne može biti roditelj detetu koje već ima roditelja u stablu.");

        super.validate();
    }
}
