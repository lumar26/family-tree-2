package validation.impl.structural.concrete;

import model.Entity;
import model.impl.Node;
import repository.Repository;
import validation.impl.structural.StructuralValidator;

import java.util.HashMap;
import java.util.Map;

public class EmptyFamilyTreeValidator extends StructuralValidator {

    private final long treeID;

    public EmptyFamilyTreeValidator(Repository repository, long treeID) {
        super(repository);

        this.treeID = treeID;
    }

    @Override
    public void validate() throws Exception {
        Map<Long, Entity> entities = repository.findUnderCondition("tree_id = " + treeID, Node.class);
        Map<Long, Node> nodes = new HashMap<>();
        entities.forEach((id, en) -> nodes.put(id, (Node) en));
        if (nodes.size() > 0)
            throw new Exception("Nije moguće obrisati stablo koje ima članove, najpre je potrebno obrisati sve članove stabla.");
        super.validate();
    }
}
