package validation.impl.structural.concrete;

import model.Entity;
import model.impl.Node;
import repository.Repository;
import validation.impl.structural.StructuralValidator;

import java.util.HashMap;
import java.util.Map;

public class RequiredFamilyMemberValidator extends StructuralValidator {

    private final long treeID;
    private final long familyMemberID;

    public RequiredFamilyMemberValidator(Repository repository, long treeID, long familyMemberID) {
        super(repository);
        this.treeID = treeID;
        this.familyMemberID = familyMemberID;
    }

    @Override
    public void validate() throws Exception {
        Map<Long, Entity> entities = repository.findUnderCondition("tree_id = " + treeID, Node.class);
        Map<Long, Node> nodes = new HashMap<>();
        entities.forEach((id, en) -> nodes.put(id, (Node) en));

        /*ako stablo ima clanove mora da postoji familyMemberNode !+ null*/
        int nodesOfTree = nodes.values().size();
        if (nodesOfTree > 0 && familyMemberID < 1) throw new Exception("Član porodice mora biti odabran i potvrđen!");

        super.validate();
    }
}
