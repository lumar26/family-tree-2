package validation.impl.structural.concrete;

import model.impl.Marriage;
import model.impl.Node;
import repository.Repository;
import validation.impl.structural.StructuralValidator;

public class SafeNodeDeletionValidator extends StructuralValidator {

    private final Node node;

    public SafeNodeDeletionValidator(Repository repository, Node node) {
        super(repository);
        this.node = node;
    }

    @Override
    public void validate() throws Exception {
        /*ne smemo da brisemo clana koji ima supruznika*/
        if (repository.findUnderCondition(
                "husband_id = " + node.getId() + " or wife_id = " + node.getId(), Marriage.class).values().size() >= 1)
            throw new Exception("Nije moguće ukloniti člana koji je u braku");

        /*ako ima roditelja ne sme da ima decu*/
        if (node.hasParent() && !node.getChildren().isEmpty())
            throw new Exception("Nije moguće obrisati člana koji ima i roditelja i decu, dolazi do cepanja rodoslova!");

        /*ako nema roditelja i ima vise od jednog deteta ne sme da se brise da ne bi doslo do cepanja stabla na vise delova*/
        if (!node.hasParent() && node.getChildren().size() > 1)
            throw new Exception("Nije moguće obrisati člana koji ima dvoje i više dece, dolazi do cepanja rodoslova!");

        super.validate();
    }
}
