package validation.impl.structural.concrete;

import model.Entity;
import model.impl.Marriage;
import repository.Repository;
import validation.impl.structural.StructuralValidator;

import java.util.Map;

public class UnmarriedMemberValidator extends StructuralValidator {

    private final long nodeID;
    private final long spouseID;

    public UnmarriedMemberValidator(Repository repository, long nodeID, long spouseID) {
        super(repository);
        this.nodeID = nodeID;
        this.spouseID = spouseID;
    }

    @Override
    public void validate() throws Exception {

        /*ako je dodeljen supruznik on ne bi smeo da ima supruznika vec*/
        if (spouseID != 0) {
            Map<Long, Entity> marriages = repository.findUnderCondition(
                    "husband_id = " + spouseID + " or wife_id = " + spouseID, Marriage.class);
            Marriage marriage = null;
            if (marriages != null && marriages.values().stream().findFirst().isPresent())
                 marriage = (Marriage) marriages.values().stream().findFirst().get();
            if (marriage != null) throw new Exception("Supružnik je već u braku!");
        }
        super.validate();
    }
}
