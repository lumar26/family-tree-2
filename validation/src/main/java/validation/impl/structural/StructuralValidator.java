package validation.impl.structural;


import repository.Repository;
import validation.Validator;

public abstract class StructuralValidator implements Validator {

    private Validator next;
    protected Repository repository;

    public StructuralValidator(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Validator setNext(Validator next) {
        this.next = next;
        return this.next;
    }

    @Override
    public void validate() throws Exception {
        if (next != null)
            next.validate();
    }
}
