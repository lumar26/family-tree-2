package validation;

public interface Validator {
    Validator setNext(Validator next);
    void validate() throws Exception;
}
