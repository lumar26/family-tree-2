package repository;

import database.ConnectionPool;
import model.Entity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public final class Repository {
    private final Connection connection;

    public Repository() {
        connection = ConnectionPool.getConnection();
    }

    public Entity save(Entity entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(entity.queryInsert(), Statement.RETURN_GENERATED_KEYS);
        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            entity.setId(rs.getLong(1));
        }
        statement.close();
        rs.close();
        return entity;
    }

    public Entity update(Entity updated, long primaryKey) throws SQLException {
        String query = updated.queryUpdate(primaryKey);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
        updated.setId(primaryKey);
        return updated;
    }

    public Map<Long, Entity> findAll(Class<? extends Entity> entityClass) throws SQLException {
        Method gueryFind = null;
        Method toEntity = null;
        try {
            Map<Long, Entity> entities = new HashMap<>();

            /*pomocu refleksije dobijamo koje to staticke metode treba da se izvrse tj. iz koje tacno konkretne klase koja nasledjuje Entity*/
            gueryFind = entityClass.getMethod("queryFindAll");
            toEntity = entityClass.getMethod("toEntity", ResultSet.class);


            String query = (String) gueryFind.invoke(null);

            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Entity entity = (Entity) toEntity.invoke(null, rs);
                entities.put(entity.getId(), entity);
            }
            statement.close();
            rs.close();
            return entities;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Entity findById(long primaryKey, Class<? extends Entity> entityClass) throws SQLException {
        Method gueryFind = null;
        Method toEntity = null;
        try {

            Entity result = null;
            /*pomocu refleksije dobijamo koje to staticke metode treba da se izvrse tj. iz koje tacno konkretne klase koja nasledjuje Entity*/
            gueryFind = entityClass.getMethod("queryFind", String.class);
            toEntity = entityClass.getMethod("toEntity", ResultSet.class);


            String query = (String) gueryFind.invoke(null, "id = " + primaryKey);

            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                result = (Entity) toEntity.invoke(null, rs);
            }
            statement.close();
            rs.close();
            return result;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Map<Long, Entity> findUnderCondition(String condition, Class<? extends Entity> entityClass) throws SQLException {
        Method gueryFind = null;
        Method toEntity = null;
        try {
            Map<Long, Entity> entities = new HashMap<>();

            /*pomocu refleksije dobijamo koje to staticke metode treba da se izvrse tj. iz koje tacno konkretne klase koja nasledjuje Entity*/
            gueryFind = entityClass.getMethod("queryFind", String.class);
            toEntity = entityClass.getMethod("toEntity", ResultSet.class);


            String query = (String) gueryFind.invoke(null, condition);

            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Entity entity = (Entity) toEntity.invoke(null, rs);
                entities.put(entity.getId(), entity);
            }
            statement.close();
            rs.close();
            return entities;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Entity delete(long primaryKey, Class<? extends Entity> entityClass) throws SQLException {
        Method queryDelete;
        try {

            /*pomocu refleksije dobijamo koje to staticke metode treba da se izvrse tj. iz koje tacno konkretne klase koja nasledjuje Entity*/
            queryDelete = entityClass.getMethod("deleteQuery", Long.class);


            String query = (String) queryDelete.invoke(null, primaryKey);
            Entity deletedRecord = findById(primaryKey, entityClass);

            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();

            statement.close();
            return deletedRecord;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Entity deleteUnderCondition(String condition, Class<? extends Entity> entityClass) throws SQLException {
        Method queryDelete;
        try {

            /*pomocu refleksije dobijamo koje to staticke metode treba da se izvrse tj. iz koje tacno konkretne klase koja nasledjuje Entity*/
            queryDelete = entityClass.getMethod("conditionalDeleteQuery", String.class);

            String query = (String) queryDelete.invoke(null, condition);
            Map<Long, Entity> map = findUnderCondition(condition, entityClass);
            Entity deletedRecord = map.values().stream().findFirst().get();

            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();

            statement.close();
            return deletedRecord;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void rollback() throws SQLException {
        connection.rollback();
        ConnectionPool.releaseConnection(connection);
    }

    public void commit() throws SQLException {
        connection.commit();
        ConnectionPool.releaseConnection(connection);
    }
}
