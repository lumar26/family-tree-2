package database;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class DatabaseProperties {
    private final Properties props;

    public DatabaseProperties() {
        this.props = new Properties();
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("db.properties");
        try {
            props.load(is);
        } catch (IOException e) {
            System.err.println("Could not load properties");
        }
    }
    //  Bill Pugh implementation of Singleton pattern
    private static class LazyHolder {
        private static final DatabaseProperties INSTANCE = new DatabaseProperties();
    }

    public static DatabaseProperties getInstance() {
        return DatabaseProperties.LazyHolder.INSTANCE;
    }

    public String getProperty(String key) {
        return props.getProperty(key);
    }

    public Set<String> getAllPropertyNames() {
        return props.stringPropertyNames();
    }

    public boolean containsKey(String key) {
        return props.containsKey(key);
    }

    public void setProperty(String key, String value){
        props.setProperty(key, value);
    }
}
