package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;

public class ConnectionPool {
    private static final LinkedList<Connection> pool = new LinkedList<>();

    static {
        String url = DatabaseProperties.getInstance().getProperty("database_url");
        String username = DatabaseProperties.getInstance().getProperty("database_user");
        String password = DatabaseProperties.getInstance().getProperty("database_password");

        String nocStr = DatabaseProperties.getInstance().getProperty("number_of_connections");
        int numberOfConnections = Integer.parseInt(nocStr);

        for (int i = 0; i < numberOfConnections; i++) {
            try {
                Connection connection = DriverManager.getConnection(url, username, password);
                connection.setAutoCommit(false);
                pool.add(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static Connection getConnection(){
        if (pool.isEmpty()){
            try {
                Thread.sleep(200);
            } catch (InterruptedException ignored) {
            }
            return getConnection();
        }
        Connection conn = pool.pop();
        return conn;
    }

    public static boolean releaseConnection(Connection connection){
        return pool.add(connection);
    }
}
