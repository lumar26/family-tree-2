package client;

import network.Request;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Sender {

    private final ObjectOutputStream toServer;

    public Sender(Socket s) throws IOException {
        toServer = new ObjectOutputStream(s.getOutputStream());
    }

    public void sendRequest(Request request) throws Exception {
        try {
            toServer.writeObject(request);
            toServer.flush();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }


}
