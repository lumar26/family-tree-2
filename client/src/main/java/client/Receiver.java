package client;

import network.Response;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class Receiver implements Runnable{

    private boolean running;
    private ObjectInputStream fromServer;

    public Receiver(Socket socket) throws IOException {
        fromServer = new ObjectInputStream((socket.getInputStream()));
    }

    @Override
    public void run() {
        try {
            while (running) {
                Response response = readResponse();
                System.out.println(response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Response readResponse() throws Exception {
        try {
            return  (Response) fromServer.readObject();
        } catch (ClassNotFoundException | IOException e) {
            throw new Exception("Nije moguće izvršiti zahtev, nije stigao odgovor od servera.");
        }
    }

    public void terminate() throws IOException {
        running =  false;
        fromServer.close();
    }

}
