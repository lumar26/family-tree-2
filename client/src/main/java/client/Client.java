package client;

import network.Request;
import network.RequestType;
import network.Response;
import network.ResponseStatus;
import prop.TransferProperties;

import javax.swing.*;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

public class Client {
    protected Sender sender;
    protected Receiver receiver;

    private static Client instance;

    public static Client getInstance() {
        if (instance == null) instance = new Client();
        return instance;
    }

    private Client() {
        try {
            Socket socket = new Socket(TransferProperties.getInstance().getProperty("server_name"),
                    Integer.parseInt(TransferProperties.getInstance().getProperty("server_port")));
            sender = new Sender(socket);
            receiver = new Receiver(socket);
        } catch (ConnectException ce) {
            JOptionPane.showMessageDialog(null, "Server nedostupan, pokušajte kasnije.", "Greška", JOptionPane.ERROR_MESSAGE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda koja kreira zahtev za server, šalje ga na mrežu i zatim osluškuje mrežu za odgovorom, vraća odgovor koji je došao od servera
     *
     * @param data Podaci koji se stavljaju u zahtev
     * @param type Tip zahteva koji se šalje serveru
     * @return Podaci koje je server vratio u slučaju da je operacija izvršena uspešno
     * @throws Exception Ako operacija nije uspešno izvršena dešava se izuzetak koji sa sobom nosi informaciju zbog čega se dogodio
     */
    public Object makeRequest(Object data, RequestType type) throws Exception {
        Request request = new Request(data, type);

        sender.sendRequest(request);
        Response response = receiver.readResponse();
        if (response.getStatus().equals(ResponseStatus.ERROR)) throw response.getError();
        return response.getData();
    }

    public Sender getSender() {
        return sender;
    }

    public Receiver getReceiver() {
        return receiver;
    }
}
