package controller;

import client.Client;
import dto.impl.*;
import dto.impl.kinship.KinshipDTO;
import mapper.MemberDtoMapper;
import model.enums.Gender;
import network.RequestType;
import view.component.MemberInfoPanel;
import view.component.SpousePanel;

import javax.swing.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class NodeController {
    private static NodeController instance;
    private final Client client = Client.getInstance();


    public static NodeController getInstance() {
        if (instance == null) instance = new NodeController();
        return instance;
    }

    /**
     * @param memberInfoPanel Panel sa forme sa kog se kupe podaci od kojih se pravi objekat MemberInfoDTO
     * @param spousePanel Panel sa forme koji sadrži podatke o supružniku
     * @param cmbNodes Combo box iz kog se bira član koji se menja
     * @return Osnovni podaci o ažuriranom članu
     * @throws Exception Ukoliko nije moguće ažurirati člana, sa porukom o grešci
     */
    public NodeDTO updateNode(MemberInfoPanel memberInfoPanel, SpousePanel spousePanel, JComboBox<NodeUpdateDTO> cmbNodes) throws Exception {
        MemberInfoDTO updatedMember = createMember(memberInfoPanel);
        NodeUpdateDTO selectedNode = (NodeUpdateDTO) cmbNodes.getSelectedItem();
        if (selectedNode == null) throw new Exception("Nije izabran član za izmenu.");


        updatedMember.setId(selectedNode.getMemberData().getId()); //postavljamo id od Member objekta koji vec postoji u bazi za dati cvor

        NodeDTO spouse = spousePanel.getSelectedSpouse() != null ? spousePanel.getSelectedSpouse() : null;
        NodeUpdateDTO nodeToUpdate = new NodeUpdateDTO(
                selectedNode.getNodeId(),
                updatedMember,
                spouse == null ? 0 : spouse.getNodeID(),
                selectedNode.getTreeID(), selectedNode.getUserID(),
                spouse == null ? null : spouse.getDisplayText()
        );
        return (NodeDTO) client.makeRequest(nodeToUpdate, RequestType.UPDATE_NODE);
    }

    /**
     * @param memberInfoPanel Panel sa forme sa kog se kupe podaci od kojih se pravi objekat MemberInfoDTO
     * @return Kreirani objekat MemberInfoDTO
     * @throws Exception Ukoliko su podaci na formi loše uneti
     */
    private MemberInfoDTO createMember(MemberInfoPanel memberInfoPanel) throws Exception {

        String name = memberInfoPanel.getTxtName().getText().trim();
        String surname = memberInfoPanel.getTxtSurname().getText().trim();

        LocalDate birthDate = toLocalDate(memberInfoPanel.getTxtBirthDate().getValue());
        String birthPLace = memberInfoPanel.getTxtBirthPlace().getText();
        Gender gender;
        if (memberInfoPanel.getCheckMale().isSelected()) gender = Gender.MALE;
        else gender = Gender.FEMALE;

        LocalDate deathDate = null;
        String address = null;
        String deathPLace = null;

        if (memberInfoPanel.getCheckAlive().isSelected()){
            address = memberInfoPanel.getTxtAddress().getText();
        } else {
            deathPLace = memberInfoPanel.getTxtDeathPlace().getText();
            deathDate = toLocalDate(memberInfoPanel.getTxtDeathDate().getValue());
        }

        String biographyUrl = memberInfoPanel.getTxtUrlBiography().getText();
        String pictureUrl = memberInfoPanel.getTxtUrlPicture().getText();

        /*String name, String surname, Gender gender,
                                        LocalDate birthDate, LocalDate deathDate, String address,
                                        String birthPlace, String deathPlace, String biographyURL, String pictureURL*/
        MemberInfoDTO m = MemberDtoMapper.factory(name, surname, gender, birthDate, deathDate, address, birthPLace, deathPLace,  biographyUrl, pictureUrl);
        System.out.println("Node controller napravljen memberInfo> " + m);
        return m;
    }

    private LocalDate toLocalDate(Object value) {
        try {
            if (value == null) return null;
            return ((Date) value).toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        } catch (UnsupportedOperationException e) {
            return ((java.sql.Date) value).toLocalDate();
        }
    }

    /**
     * @param tree Rodoslov iz kog se biraju članovi za ažuriranje
     * @return Listu čvorovoa sa podacima koji su trenutno sačuvani a koji mogu biti izmenjeni
     * @throws Exception Ako ne postoje članovi stabla
     */
    public NodeUpdateDTO[] getNodesForUpdate(FamilyTreeDTO tree) throws Exception {
        return (NodeUpdateDTO[]) client.makeRequest(tree, RequestType.GET_NODES_FOR_UPDATE);
    }

    /**
     * @param nodeToDelete Čvor koji je korisnik odredio za brisanje
     * @return Osnovni podaci o obrisanom čvoru
     * @throws Exception Ako nije moguće obrisati člana iz stabla
     */
    public NodeDTO deleteNode(NodeDTO nodeToDelete) throws Exception {
        return (NodeDTO) client.makeRequest(nodeToDelete, RequestType.DELETE_NODE);
    }

    /**
     * @param selectedTree Rodoslov koji je korisnik izabrao i čiji se članovi vraćaju
     * @return Lista čvorova koji predstavljaju članove rodoslova
     * @throws Exception Ako rodoslov nema članove
     */
    public NodeDTO[] getNodesOfTree(FamilyTreeDTO selectedTree) throws Exception {
        return (NodeDTO[]) client.makeRequest(selectedTree.getTreeID(), RequestType.GET_NODES_OF_TREE);
    }

    /**
     * @param familyTree Rodoslov u kome se čuva novi član
     * @param ancestry Lista predaka člana koji se unosi u formi stringa
     * @param newMember Podaci o članu koji se unosi
     * @param familyMemberNode Čvor koji predstavlja člana porodice novog člana, roditelj ili dete
     * @param spouse Supružnik, ukoliko je izabran
     * @param selectedRole Uloga člana porodice koji je odabran, roditelj ili dete
     * @param user Korisnik koji unosi člana
     * @return Vraaća osnovne podatke o unetom članu
     * @throws Exception Ukoliko nije moguće sačuvati novi čvor / člana
     */
    public NodeDTO saveNewNode(FamilyTreeDTO familyTree, String ancestry, MemberInfoDTO newMember, NodeDTO familyMemberNode, NodeDTO spouse, String selectedRole, UserDTO user) throws Exception {
        if (familyMemberNode == null && getNodesOfTree(familyTree).length > 0)
            throw new Exception("Član porodice (roditelj ili dete) mora biti definisan pri unosu novog člana u stablo");

        NodeSaveDTO newNode = new NodeSaveDTO(familyTree.getTreeID(), ancestry, user.getUserId(), spouse == null ? 0 : spouse.getNodeID(),
                familyMemberNode == null ? null : new FamilyMemberDTO(familyMemberNode.getNodeID(), selectedRole), newMember);

        return (NodeDTO) client.makeRequest(newNode, RequestType.SAVE_NODE);
    }

    /**
     * @param node1 Prvi izabrani član
     * @param node2 Drugi izabrani član
     * @return Detaljni podaci o srodstvu, tipu srodstva, udaljenosti, itd.
     * @throws Exception Ukoliko za dva člana nije moguće utvrditi srodstvo, prikazuje se poruka o grešci
     */
    public KinshipDTO getKinship(NodeDTO node1, NodeDTO node2) throws Exception {
        return (KinshipDTO) client.makeRequest(new NodeDTO[]{node1, node2}, RequestType.FIND_KINSHIP);
    }

    /**
     * @param familyTree Rodoslov koji je korisnik odabrao za prikaz
     * @return Vraća koreni čvor stabla koji sadrži reference ka svim svojim potomcima
     * @throws Exception Ako nije moguće naći članove u stablu
     */
    public TreeNodeDTO getFamilyTreeRoot(FamilyTreeDTO familyTree) throws Exception {
        return (TreeNodeDTO) client.makeRequest(familyTree.getTreeID(), RequestType.GET_FAMILY_TREE_ROOT);
    }

    /**
     * @param node Izabrani čvor prikazanog stabla
     * @return Vraća podatke o izabranom članu rodoslova gde se nalaze osnovni podaci kao i podaci o supružniku
     * @throws Exception Ukoliko nije moguće pronaći informacije o članu, prikazuje se poruka o grešci
     */
    public NodeInfoDTO getNodeInfo(TreeNodeDTO node) throws Exception {
        return (NodeInfoDTO) client.makeRequest(node, RequestType.GET_NODE_INFO);
    }
}
