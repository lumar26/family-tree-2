package controller;

import client.Client;
import dto.impl.FamilyTreeDTO;
import dto.impl.FamilyTreeDataDTO;
import dto.impl.UserDTO;
import network.RequestType;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class FamilyTreeController {
    private static FamilyTreeController instance;
    private final Client client = Client.getInstance();

    public static FamilyTreeController getInstance() {
        if (instance == null) instance = new FamilyTreeController();
        return instance;
    }

    public FamilyTreeDTO[] getAllFamilyTrees() throws Exception {
        return (FamilyTreeDTO[]) client.makeRequest(null, RequestType.GET_ALL_TREES);
    }

    /**
     * @param name Ime rodoslova
     * @param description Opis rodoslova
     * @param startDate Datum od kad se rodoslov posmatra
     * @param currentUser Korinsik koji kreira rodoslov
     * @return Kreirani rodoslov u obliku DTO
     * @throws Exception Greška prilikom kreiranja ili unosa novog rodoslova u bazu
     */
    public FamilyTreeDTO saveFamilyTree(String name, String description, Object startDate, UserDTO currentUser) throws Exception {
        if (name == null || name.isEmpty() || name.isBlank())
            throw new Exception("Naziv rodoslova mora biti definisan.");

        LocalDate date;
        try {
            Date temp = (Date) startDate;
            date = temp.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (Exception e) {
            throw new Exception("Datum početka praćenja rodoslova nije unet u odgovarajućem formatu");
        }

        FamilyTreeDataDTO tree = new FamilyTreeDataDTO(name, description, date, currentUser.getUserId());
        return (FamilyTreeDTO) client.makeRequest(tree, RequestType.SAVE_FAMILY_TREE);

    }

    /**
     * @param user Korisnik čije rodoslova treba vratiti
     * @return Lista rodoslova prosleđenog korisnika
     * @throws Exception Ukoliko nije moguće pronaći rodoslove datog člana
     */
    public FamilyTreeDTO[] getTreesOfUser(UserDTO user) throws Exception {
        return (FamilyTreeDTO[]) client.makeRequest(user, RequestType.GET_TREES_OF_USER);
    }

    /**
     * @param selectedTree Stablo koje je korinsik izabrao iz padajuće liste
     * @return Vraća podatke o obrisanom rodoslovu u obliku DTO
     * @throws Exception Ukoliko se rodoslov iz nekog razloga ne može obrisati
     */
    public FamilyTreeDTO deleteFamilyTree(FamilyTreeDTO selectedTree) throws Exception {
        return (FamilyTreeDTO) client.makeRequest(selectedTree, RequestType.DELETE_FAMILY_TREE);
    }


}
