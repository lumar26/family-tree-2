package controller;

import client.Client;
import dto.impl.UserDTO;
import dto.impl.UserLoginDTO;
import network.RequestType;
import network.Request;

import java.io.IOException;

public class UserController {
    private static UserController instance;
    private final Client client = Client.getInstance();

    public static UserController getInstance() {
        if (instance == null)
            instance = new UserController();
        return instance;
    }

    /**
     * @param username Korisničko ime koj je korinsik uneo na formi za prijavljivanje
     * @param password Lozinka koju je korisnik uneo na formi za prijavljivanje
     * @return Ukoliko korisnik sa datim korinsičkim imenom i lozinkom postoji vraća se korisnik u obliku DTO-a
     * @throws Exception Ukoliko ne postoji korisnik sa datim kredencijalima ili je došlo do druge greške na serveru
     */
    public UserDTO login(String username, String password) throws Exception {
        UserLoginDTO userLogin = new UserLoginDTO(username, password);
        return (UserDTO) client.makeRequest(userLogin, RequestType.LOGIN);
    }

    /**
     * Metoda se poziva u trenutku kada korisnik izađe iz aplikacije
     *
     * @throws IOException U slučaju greške prilikom zatvaranja soketa u klasi Client
     */
    public void logout() throws IOException {
        try {
            client.getSender().sendRequest(new Request(null, RequestType.LOGOUT));
            System.out.println("Odjava...");
            client.getReceiver().terminate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
