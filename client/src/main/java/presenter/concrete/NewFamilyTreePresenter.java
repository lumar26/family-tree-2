package presenter.concrete;

import dto.impl.FamilyTreeDTO;
import dto.impl.UserDTO;
import presenter.Presenter;
import view.form.MainForm;
import view.form.NewFamilyTreeDialog;

public class NewFamilyTreePresenter extends Presenter {

    private final NewFamilyTreeDialog dialog;

    public NewFamilyTreePresenter(MainForm mainForm, UserDTO user) {
        super(user);
        form = new NewFamilyTreeDialog(mainForm, true);
        dialog = (NewFamilyTreeDialog) form;
        prepareForm();
        setListeners();
    }

    @Override
    protected void setListeners() {
        dialog.getBtnSaveFamilyTree().addActionListener(a -> saveFamilyTree());
    }

    private void saveFamilyTree() {
        try {
            FamilyTreeDTO familyTree = familyTreeController.saveFamilyTree(dialog.getTxtName().getText()
                    , dialog.getTxtDescription().getText()
                    , dialog.getDateStartDate().getValue()
                    , user);
            dialog.printInfo("Uspešno sačuvan rodoslov: " + familyTree.toString());
            form.dispose();
        } catch (Exception e) {
            dialog.printError(e);
        }
    }

    @Override
    protected void prepareForm() {
    }
}
