package presenter.concrete;

import dto.impl.FamilyTreeDTO;
import dto.impl.NodeDTO;
import dto.impl.UserDTO;
import presenter.Presenter;
import view.form.DeleteMemberDialog;
import view.form.MainForm;

import javax.swing.*;
import java.awt.event.ItemEvent;

public class DeleteMemberPresenter extends Presenter {

    private final DeleteMemberDialog dialog;

    private NodeDTO nodeToDelete;
    private FamilyTreeDTO selectedTree;

    public DeleteMemberPresenter(MainForm mainForm, UserDTO user) {
        super(user);
        form = new DeleteMemberDialog(mainForm, true);
        dialog = (DeleteMemberDialog) form;
        prepareForm();
        setListeners();
    }

    @Override
    protected void setListeners() {
        dialog.getCmbFamilyTree().addItemListener(this::selectFamilyTree);

        dialog.getCmbNodes().addItemListener(this::selectMemberToDelete);

        dialog.getBtnDeleteNode().addActionListener(a -> deleteMember());
    }

    private void deleteMember() {
        if (selectedTree == null)
            dialog.printError("Morate odabrati stablo iz kog brišete člana\nMorate odabrati člana za odabrano stablo");
        if (nodeToDelete == null)
            dialog.printError("Morate odabrati člana za odabrano stablo");
        NodeDTO deleted = null;
        try {
            deleted = nodeController.deleteNode(nodeToDelete);
            dialog.printInfo("Član uspešno obrisan: " + deleted.toString());
            form.dispose();
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }
    }

    private void selectMemberToDelete(ItemEvent i) {
        if (i.getStateChange() == ItemEvent.SELECTED) {
            try {
                nodeToDelete = (NodeDTO) dialog.getCmbNodes().getSelectedItem();
            } catch (Exception e) {
                dialog.printError(e.getMessage());
            }
        }
    }

    private void selectFamilyTree(ItemEvent itemChange) {
        if (itemChange.getStateChange() == ItemEvent.SELECTED) {
            try {
                selectedTree = (FamilyTreeDTO) dialog.getCmbFamilyTree().getSelectedItem();
                dialog.getCmbNodes().setModel(new DefaultComboBoxModel<>(nodeController.getNodesOfTree(selectedTree)));
                dialog.getCmbNodes().setSelectedIndex(-1);
            } catch (Exception e) {
                dialog.printError(e.getMessage());
            }
        }
    }

    @Override
    protected void prepareForm() {
        try {
            FamilyTreeDTO[] treeDTOS = familyTreeController.getTreesOfUser(user);
            dialog.getCmbFamilyTree().setModel(new DefaultComboBoxModel<>(treeDTOS));
            dialog.getCmbFamilyTree().setSelectedIndex(-1);
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }
    }
}
