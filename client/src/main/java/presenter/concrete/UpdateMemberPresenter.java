package presenter.concrete;

import controller.FamilyTreeController;
import controller.NodeController;
import dto.impl.FamilyTreeDTO;
import dto.impl.NodeDTO;
import dto.impl.NodeUpdateDTO;
import dto.impl.UserDTO;
import presenter.Presenter;
import view.form.MainForm;
import view.form.UpdateMemberDialog;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.util.Arrays;

public class UpdateMemberPresenter extends Presenter {
    private final UpdateMemberDialog dialog;

    private final NodeController nodeController;
    private final FamilyTreeController familyTreeController;

    public UpdateMemberPresenter(MainForm mainForm, UserDTO user) {
        super(user);
        this.nodeController = NodeController.getInstance();
        this.familyTreeController = FamilyTreeController.getInstance();

        form = new UpdateMemberDialog(mainForm, true);
        dialog = (UpdateMemberDialog) form;
        setListeners();
        prepareForm();
    }

    public void prepareForm() {
        try {
            FamilyTreeDTO[] trees = familyTreeController.getAllFamilyTrees();
            dialog.getCmbFamilyTree().setModel(new DefaultComboBoxModel<>(trees));
            dialog.getCmbFamilyTree().setSelectedIndex(-1);
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }

    }

    public void setListeners() {
        dialog.getCmbFamilyTree().addItemListener(this::selectFamilyTree);
        dialog.getBtnUpdate().addActionListener(evt -> updateMember());
        dialog.getCmbNodes().addItemListener(this::selectMemberToUpdate);
    }

    private void selectFamilyTree(ItemEvent itemEvent) {
        if (itemEvent.getStateChange() == ItemEvent.SELECTED) {
            FamilyTreeDTO selectedTree = (FamilyTreeDTO) dialog.getCmbFamilyTree().getSelectedItem();
            if (selectedTree == null) return;

            NodeUpdateDTO[] nodes = new NodeUpdateDTO[0];
            try {
                nodes = nodeController.getNodesForUpdate(selectedTree);
                if (nodes.length == 0) {
                    dialog.printError("Izabrani rodoslov ne sadrži članove");
                    return;
                }
            } catch (Exception e) {
                dialog.printError(e.getMessage());
            }
            dialog.getCmbNodes().setModel(new DefaultComboBoxModel<>(nodes));
            dialog.getCmbNodes().setSelectedIndex(-1);

            /*dodavanje stabal odakle moze da se odabere supruga*/
            try {
//                necemo stablo koje je izabrano
                FamilyTreeDTO[] trees = Arrays.stream(familyTreeController.getAllFamilyTrees()).
                        filter(tree -> tree.getTreeID() != selectedTree.getTreeID()).
                        toArray(FamilyTreeDTO[]::new);
                dialog.getSpousePanel().getCmbFamilyTrees().setModel(new DefaultComboBoxModel<>(trees));
            } catch (Exception e) {
                dialog.printError(e.getMessage());
                dialog.dispose();
            }
        }
    }

    private void selectMemberToUpdate(ItemEvent itemChanged) {
        if (itemChanged.getStateChange() == ItemEvent.SELECTED) {
            NodeUpdateDTO selectedNode = (NodeUpdateDTO) dialog.getCmbNodes().getSelectedItem();
            if (selectedNode == null) return;
            dialog.fillMemberInfo(selectedNode.getMemberData());

            /*ako odabrani clan ima supruznika on se prikazuje, ako ne onda se nudi mogucnost unosa supruznika*/
            if (selectedNode.getSpouseID() != 0 && selectedNode.getSpouseInfo() != null){
                dialog.getSpousePanel().showExistingSpouse(selectedNode.getSpouseInfo());
            } else dialog.getSpousePanel().enableSpouseAdding();
        }
    }

    private void updateMember() {
        try {
            NodeDTO updated = nodeController.updateNode(dialog.getMemberInfoPanel(), dialog.getSpousePanel(), dialog.getCmbNodes());
            JOptionPane.showMessageDialog(dialog, "Uspešno ažuriranje člana:  " + updated);
            dialog.dispose();
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }
    }
}
