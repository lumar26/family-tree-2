package presenter.concrete;

import controller.UserController;
import dto.impl.UserDTO;
import presenter.Presenter;
import view.form.*;

import javax.swing.*;

public class MainPresenter extends Presenter {
    private final UserController userController = UserController.getInstance();
    private final LoginForm loginForm;
    private MainForm mainForm;

    private UserDTO user;


    public MainPresenter() {
        super(null);
        form = new LoginForm(null, true);
        loginForm = (LoginForm) form;
        loginForm.getBtnLogin().addActionListener(actionEvent -> loginUser());
    }

    private void loginUser() {
        try {
            user = userController.login(loginForm.getTxtUsername().getText(), new String(loginForm.getTxtPassword().getPassword()));
            loginForm.dispose();
            JOptionPane.showMessageDialog(loginForm, "Uspešno ste se prijavili na sistem", "Poruka", JOptionPane.INFORMATION_MESSAGE);

            mainForm = new MainForm(user);
            setListeners();
            prepareForm();
            mainForm.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(loginForm, e.getMessage(), "Greška", JOptionPane.ERROR_MESSAGE);
        }
    }


    @Override
    protected void setListeners() {
        mainForm.getOptionChangeMember().addActionListener(actionEvent -> (new UpdateMemberPresenter(mainForm, user)).show());

        mainForm.getOptionCreateFamilyTree().addActionListener(actionEvent -> (new NewFamilyTreePresenter(mainForm, user)).show());

        mainForm.getOptionDeleteFamilyTree().addActionListener(actionEvent -> (new DeleteFamilyTreePresenter(mainForm, user)).show());

        mainForm.getOptionShowFamilyTree().addActionListener(actionEvent -> (new ShowFamilyTreePresenter(mainForm, user)).show());

        mainForm.getOptionNewMember().addActionListener(actionEvent -> (new NewMemberPresenter(mainForm, user)).show());

        mainForm.getOptionDeleteMember().addActionListener(actionEvent -> (new DeleteMemberPresenter(mainForm, user)).show());

        mainForm.getOptionFindRelationship().addActionListener(actionEvent -> (new FindRelationshipPresenter(mainForm, user)).show());
    }

    @Override
    protected void prepareForm() {
        mainForm.getLblLoggedUser().setText("Prijavljeni korisnik: " + user.toString());
    }
}
