package presenter.concrete;

import dto.impl.FamilyTreeDTO;
import dto.impl.MemberInfoDTO;
import dto.impl.NodeDTO;
import dto.impl.UserDTO;
import mapper.MemberDtoMapper;
import model.enums.Gender;
import presenter.Presenter;
import view.form.MainForm;
import view.form.NewMemberDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class NewMemberPresenter extends Presenter {
    private final NewMemberDialog dialog;

    private NodeDTO familyMember;
    private String selectedRole;
    private FamilyTreeDTO familyTree;

    public NewMemberPresenter(MainForm mainForm, UserDTO user) {
        super(user);
        form = new NewMemberDialog(mainForm, true);
        dialog = (NewMemberDialog) form;
        setListeners();
        prepareForm();
    }

    protected void prepareForm() {
        try {
            FamilyTreeDTO[] allTrees = familyTreeController.getAllFamilyTrees();
            dialog.getCmbFamilyTree().setModel(new DefaultComboBoxModel<>(allTrees));
            dialog.getCmbFamilyTree().setSelectedIndex(-1);
            dialog.getSpousePanel().setTrees(allTrees); // postavljanje stabala i u panelu za dodavanje supruge
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }
    }

    protected void setListeners() {
//        ako cekiramo da je clan ziv
        dialog.getCheckAlive().addActionListener(actionEvent -> {
            dialog.getLayerDeadAlive().setVisible(true);
            dialog.getPnlSpecificAlive().setVisible(true);
            dialog.getPnlSpecificDead().setVisible(false);
        });
//    ako cekiramo da je clan mrtav
        dialog.getCheckDead().addActionListener(actionEvent -> {
            dialog.getLayerDeadAlive().setVisible(true);
            dialog.getPnlSpecificAlive().setVisible(false);
            dialog.getPnlSpecificDead().setVisible(true);
        });

//        kad ocemo da promenimo dodatog clana porodice
        dialog.getBtnChangeFamilyMember().addActionListener(a -> {
            dialog.getPnlFamilyMemberAdded().setVisible(false);
            dialog.getPnlFamilyMembers().setVisible(true);
        });

        dialog.getCmbFamilyTree().addItemListener(this::treeSelected);

        dialog.getBtnAddFamilyMember().addActionListener(a -> familyMemberAdded());

        dialog.getBtnAddMember().addActionListener(a -> saveMember());
    }

    private void saveMember() {
        try {
            MemberInfoDTO newMember = createMemberInfoDTO();
            String ancestry = "/";
            NodeDTO saved = nodeController.saveNewNode(this.familyTree, ancestry, newMember,
                    familyMember, dialog.getSpousePanel().getSelectedSpouse(), selectedRole, user);
            dialog.printInfo("Uspešno dodat član: " + saved.toString());
            dialog.dispose();
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }
    }

    private void familyMemberAdded() {
        familyMember = (NodeDTO) dialog.getCmbFamilyMember().getSelectedItem();
        selectedRole = (String) dialog.getCmbRole().getSelectedItem();

        dialog.getLblAddedMember().setText("Unet je član porodice: " + this.familyMember.getDisplayText() + ", čija je uloga: " + selectedRole);

        /*menjamo polozaj panela*/
        dialog.getPnlFamilyMembers().setVisible(false);
        dialog.getPnlFamilyMemberAdded().setVisible(true);
    }

    private void treeSelected(ItemEvent itemEvent) {
        if (itemEvent.getStateChange() == ItemEvent.SELECTED) {
            try {
                this.familyTree = (FamilyTreeDTO) dialog.getCmbFamilyTree().getSelectedItem();
                NodeDTO[] nodes = nodeController.getNodesOfTree(familyTree);

                dialog.setPreferredSize(new Dimension(1458, 578));

                dialog.getPnlNewMemberData().setVisible(true);
                dialog.getLayersAddFamilyMember().setVisible(true);
                dialog.getPnlAddButton().setVisible(true);
                dialog.getPnlSpouse().setVisible(true);

                dialog.getLayersAddFamilyMember().setVisible(false);
                dialog.getCheckMale().setSelected(true);
                dialog.getLayerDeadAlive().setVisible(false);

                try {
                    dialog.getCmbRole().setModel(new DefaultComboBoxModel<>(new String[]{"Roditelj", "Dete"}));
                    dialog.getCmbRole().setSelectedIndex(-1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (nodes.length != 0) {
                    dialog.getLayersAddFamilyMember().setVisible(true);
                    dialog.getPnlFamilyMemberAdded().setVisible(false);
                    dialog.getPnlFamilyMembers().setVisible(true);
                    dialog.getCmbFamilyMember().setModel(new DefaultComboBoxModel<>(nodes));
                } else {
                    dialog.printInfo("U stablu još uvek nema članova, ovaj član će biti prvi predstavnik stabla");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private MemberInfoDTO createMemberInfoDTO() throws Exception {
        Gender gender = dialog.getCheckMale().isSelected() ? Gender.MALE : Gender.FEMALE;

        String name = dialog.getTxtName().getText().trim();
        String surname = dialog.getTxtSurname().getText().trim();
        String birthPlace = dialog.getTxtBirthPlace().getText().trim();
        String deathPlace = null;
        String address = null;

        String biographyURL = dialog.getTxtUrlBiography().getText().trim();
        String pictureURL = dialog.getTxtUrlPicture().getText().trim();


        LocalDate birthDate;
        try {
            birthDate = ((Date) dialog.getTxtBirthDate().getValue()).toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        } catch (Exception e) {
            dialog.printError("Datum rođenja mora biti unesen u naznačenom formatu.");
            return null;
        }
        LocalDate deathDate = null;

        if (dialog.getCheckAlive().isSelected()) {
            address = dialog.getTxtAddress().getText().trim();
        }

        if(dialog.getCheckDead().isSelected()) {
            deathPlace = dialog.getTxtDeathPlace().getText().trim();
            try {
                deathDate = ((Date) dialog.getTxtDeathDate().getValue()).toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
            } catch (Exception e) {
                dialog.printError("Datum smrti mora biti unet u zahtevanom formatu");
            }
        }

        return MemberDtoMapper.factory(name, surname, gender, birthDate, deathDate,
                address, birthPlace, deathPlace, biographyURL, pictureURL);
    }
}
