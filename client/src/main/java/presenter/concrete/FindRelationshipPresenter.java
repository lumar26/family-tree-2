package presenter.concrete;

import controller.FamilyTreeController;
import controller.NodeController;
import dto.impl.FamilyTreeDTO;
import dto.impl.NodeDTO;
import dto.impl.UserDTO;
import dto.impl.kinship.KinshipDTO;
import dto.impl.kinship.concrete.LateralKinshipDTO;
import dto.impl.kinship.concrete.VerticalKinshipDTO;
import model.enums.KinshipType;
import presenter.Presenter;
import view.form.FindRelationshipDialog;
import view.form.MainForm;

import javax.swing.*;
import java.awt.event.ItemEvent;

public class FindRelationshipPresenter extends Presenter {

    private final FindRelationshipDialog dialog;
    private final NodeController nodeController;
    private final FamilyTreeController familyTreeController;

    private final UserDTO user;

    public FindRelationshipPresenter(MainForm mainForm, UserDTO user) {
        super(user);
        nodeController = NodeController.getInstance();
        familyTreeController = FamilyTreeController.getInstance();
        this.user = user;
        form = new FindRelationshipDialog(mainForm, true);
        dialog = (FindRelationshipDialog) form;
        prepareForm();
        setListeners();
    }

    @Override
    protected void setListeners() {
        dialog.getCmbFamilyTrees().addItemListener(this::selectFamilyTree);
        dialog.getBtnFindRelationship().addActionListener(a -> findKinship());
    }

    private void findKinship() {
        NodeDTO node1 = (NodeDTO) dialog.getCmbMember1().getSelectedItem();
        NodeDTO node2 = (NodeDTO) dialog.getCmbMember2().getSelectedItem();

        try {
            KinshipDTO kinship = nodeController.getKinship(node1, node2);
            if (kinship.getKinshipType().equals(KinshipType.LATERAL)) {
                LateralKinshipDTO k = (LateralKinshipDTO) kinship;
                String sameLevel;
                if (k.areCousins()) sameLevel = "jesu";
                else
                    sameLevel = "nisu";
                dialog.getTxtRelationship().setText(
                        String.format("Odabrani članovi su pobočni rođaci i %s na istom nivou* u stablu.\n*jednaka udaljenost od zajedničkog pretka - ista generacija potomaka", sameLevel));
                return;
            }

            if (kinship.getKinshipType().equals(KinshipType.VERTICAL)) {
                VerticalKinshipDTO k = (VerticalKinshipDTO) kinship;

                dialog.getTxtRelationship().setText(
                        String.format("Član %s je u ovoj rodbinskoj vezi potomak člana %s\n Član %s je članu %s %s",
                                k.getDescendant(), k.getAncestor(), k.getAncestor(), k.getDescendant(), k.getAncestorRole()));
            }
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }
    }

    private void selectFamilyTree(ItemEvent ie) {
        if (ie.getStateChange() == ItemEvent.SELECTED) {
            FamilyTreeDTO selectedTree = (FamilyTreeDTO) dialog.getCmbFamilyTrees().getSelectedItem();
            if (selectedTree == null) return;
            try {
                NodeDTO[] nodes = nodeController.getNodesOfTree(selectedTree);
                if (nodes == null || nodes.length == 0) {
                    dialog.printError("Izabrani rodoslov nema članove");
                    return;
                }
                dialog.fillMemberCombos(nodes);
            } catch (Exception e) {
                dialog.printError(e.getMessage());
            }
        }
    }

    @Override
    protected void prepareForm() {
        try {
            dialog.getCmbFamilyTrees().setModel(new DefaultComboBoxModel<>(familyTreeController.getAllFamilyTrees()));
            dialog.getCmbFamilyTrees().setSelectedIndex(-1);
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }
    }
}
