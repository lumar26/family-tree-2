package presenter.concrete;

import dto.impl.FamilyTreeDTO;
import dto.impl.UserDTO;
import presenter.Presenter;
import view.form.DeleteFamilyTreeDialog;
import view.form.MainForm;

import javax.swing.*;

public class DeleteFamilyTreePresenter extends Presenter {

    private final DeleteFamilyTreeDialog dialog;

    public DeleteFamilyTreePresenter(MainForm mainForm, UserDTO user) {
        super(user);
        form = new DeleteFamilyTreeDialog(mainForm, true);
        dialog = (DeleteFamilyTreeDialog) form;
        setListeners();
        prepareForm();
    }

    @Override
    protected void setListeners() {
        dialog.getBtnDeleteFamilyTree().addActionListener(a -> {
            try {
                FamilyTreeDTO deleted = familyTreeController.deleteFamilyTree((FamilyTreeDTO) dialog.getCmbFamilyTrees().getSelectedItem());
                dialog.printMessage("Uspešno obrisan rodoslov: " + deleted.toString());
                form.dispose();
            } catch (Exception e) {
                dialog.printError(e.getMessage());
            }
        });
    }

    @Override
    protected void prepareForm() {
        try {
            dialog.getCmbFamilyTrees().setModel(new DefaultComboBoxModel<>(familyTreeController.getTreesOfUser(user)));
            dialog.getCmbFamilyTrees().setSelectedIndex(-1);
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }
    }
}
