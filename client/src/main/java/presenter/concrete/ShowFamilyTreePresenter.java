package presenter.concrete;

import controller.FamilyTreeController;
import controller.NodeController;
import dto.impl.FamilyTreeDTO;
import dto.impl.NodeInfoDTO;
import dto.impl.TreeNodeDTO;
import dto.impl.UserDTO;
import presenter.Presenter;
import view.component.NodeDisplayPanel;
import view.form.MainForm;
import view.form.ShowFamilyTreeDialog;
import view.tree_model.FamilyTreeModel;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreeSelectionModel;

public class ShowFamilyTreePresenter extends Presenter {

    private final ShowFamilyTreeDialog dialog;

    public ShowFamilyTreePresenter(MainForm mainForm, UserDTO user) {
        super(user);
        form = new ShowFamilyTreeDialog(mainForm, true);
        dialog = (ShowFamilyTreeDialog) form;
        prepareForm();
        setListeners();
    }

    @Override
    protected void setListeners() {
        dialog.getBtnShowFamilyTree().addActionListener(a -> showTree());
    }

    private void showTree() {
        try {
            FamilyTreeDTO selected  = (FamilyTreeDTO) dialog.getCmbFamilyTree().getSelectedItem();
            if (selected == null) {
                dialog.printError("Rodoslov za prikaz mora biti izabran iz padajuće liste.");
                return;
            }

            TreeNodeDTO root = nodeController.getFamilyTreeRoot(selected);
            dialog.getTreePanel().getTreeFamily().setModel(new FamilyTreeModel(root));
            dialog.getTreePanel().getTreeFamily().getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
            dialog.getTreePanel().getTreeFamily().addTreeSelectionListener(this::showMember);
            dialog.getTreePanel().setVisible(true);
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }
    }

    private void showMember(TreeSelectionEvent treeSelectionEvent) {
        JTree tree = dialog.getTreePanel().getTreeFamily();
        TreeNodeDTO node = (TreeNodeDTO) tree.getLastSelectedPathComponent();
        try {
           NodeInfoDTO nodeInfo =  nodeController.getNodeInfo(node);
           JOptionPane.showMessageDialog(dialog, new NodeDisplayPanel(nodeInfo));
        } catch (Exception e) {
            dialog.printError(e.getMessage());
        }
    }

    @Override
    protected void prepareForm() {
        try {
            dialog.getTreePanel().setVisible(false);

            dialog.getCmbFamilyTree().setModel(new DefaultComboBoxModel<>(FamilyTreeController.getInstance().getAllFamilyTrees()));
            dialog.getCmbFamilyTree().setSelectedIndex(-1);
        } catch (Exception ex) {
            dialog.printError(ex.getMessage());
        }
    }
}
