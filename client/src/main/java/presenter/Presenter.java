package presenter;

import controller.FamilyTreeController;
import controller.NodeController;
import controller.UserController;
import dto.impl.UserDTO;

import java.awt.*;

public abstract class Presenter {
    protected final UserController userController;
    protected final FamilyTreeController familyTreeController;
    protected final NodeController nodeController;

    protected Window form;
    protected final UserDTO user;

    public Presenter(UserDTO user) {
        this.user = user;
        this.userController = UserController.getInstance();
        this.familyTreeController = FamilyTreeController.getInstance();
        this.nodeController = NodeController.getInstance();
    }

    public final void show(){
        form.setVisible(true);
    }

    protected abstract void setListeners();
    protected abstract void prepareForm();
}
