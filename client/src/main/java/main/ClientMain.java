package main;

import presenter.concrete.MainPresenter;

import javax.swing.*;

public class ClientMain {
    public static void main(String[] args) {
//        JOptionPane.showMessageDialog(null, "Nije moguće sačuvati definisani rodoslov.", "Greška", JOptionPane.ERROR_MESSAGE);
        (new MainPresenter()).show();
    }
}
