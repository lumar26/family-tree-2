package view.form;

import dto.impl.FamilyTreeDTO;
import dto.impl.NodeDTO;

import javax.swing.*;

import view.component.SpousePanel;

public class NewMemberDialog extends javax.swing.JDialog {

    public NewMemberDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        prepareDialog();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grpLivingStatus = new javax.swing.ButtonGroup();
        btnGroupGender = new javax.swing.ButtonGroup();
        jLabel5 = new javax.swing.JLabel();
        pnlNewMemberData = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblSurname = new javax.swing.JLabel();
        lblGender = new javax.swing.JLabel();
        lblBirthDate = new javax.swing.JLabel();
        lblBirthPlace = new javax.swing.JLabel();
        lblBiography = new javax.swing.JLabel();
        lblPicture = new javax.swing.JLabel();
        checkAlive = new javax.swing.JCheckBox();
        checkDead = new javax.swing.JCheckBox();
        layerDeadAlive = new javax.swing.JLayeredPane();
        pnlSpecificAlive = new javax.swing.JPanel();
        lblAddress = new javax.swing.JLabel();
        txtAddress = new javax.swing.JTextField();
        pnlSpecificDead = new javax.swing.JPanel();
        lblDeathDate = new javax.swing.JLabel();
        lblDeathPlace = new javax.swing.JLabel();
        txtDeathPlace = new javax.swing.JTextField();
        txtDeathDate = new javax.swing.JFormattedTextField();
        lblDateMessage = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtSurname = new javax.swing.JTextField();
        txtBirthPlace = new javax.swing.JTextField();
        txtUrlBiography = new javax.swing.JTextField();
        txtUrlPicture = new javax.swing.JTextField();
        checkMale = new javax.swing.JCheckBox();
        checkFemale = new javax.swing.JCheckBox();
        txtBirthDate = new javax.swing.JFormattedTextField();
        lblMessage = new javax.swing.JLabel();
        pnlFamilyTree = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbFamilyTree = new javax.swing.JComboBox<>();
        layersAddFamilyMember = new javax.swing.JLayeredPane();
        pnlFamilyMembers = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        cmbFamilyMember = new javax.swing.JComboBox<>();
        btnAddFamilyMember = new javax.swing.JButton();
        lblUloga = new javax.swing.JLabel();
        cmbRole = new javax.swing.JComboBox<>();
        lblInstruction = new javax.swing.JLabel();
        pnlFamilyMemberAdded = new javax.swing.JPanel();
        btnChangeFamilyMember = new javax.swing.JButton();
        lblAddedMember = new javax.swing.JLabel();
        pnlSpouse = new javax.swing.JPanel();
        spousePanel = new view.component.SpousePanel();
        pnlAddButton = new javax.swing.JPanel();
        btnAddMember = new javax.swing.JButton();

        jLabel5.setText("jLabel5");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Unos novog člana u rodoslov");

        pnlNewMemberData.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Podaci o članu"));

        lblName.setText("Ime člana:");

        lblSurname.setText("Prezime člana:");

        lblGender.setText("Pol:");

        lblBirthDate.setText("Datum rođenja:");

        lblBirthPlace.setText("Mesto rođenja:");

        lblBiography.setText("URL ka biografiji:");

        lblPicture.setText("URL ka slici:");

        grpLivingStatus.add(checkAlive);
        checkAlive.setText("Član je živ");

        grpLivingStatus.add(checkDead);
        checkDead.setText("Član je preminuo");

        lblAddress.setText("Trenutna adresa člana:");

        javax.swing.GroupLayout pnlSpecificAliveLayout = new javax.swing.GroupLayout(pnlSpecificAlive);
        pnlSpecificAlive.setLayout(pnlSpecificAliveLayout);
        pnlSpecificAliveLayout.setHorizontalGroup(
            pnlSpecificAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpecificAliveLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(lblAddress)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAddress)
                .addContainerGap())
        );
        pnlSpecificAliveLayout.setVerticalGroup(
            pnlSpecificAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpecificAliveLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSpecificAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAddress)
                    .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(82, Short.MAX_VALUE))
        );

        lblDeathDate.setText("Datum smrti:");

        lblDeathPlace.setText("Mesto smrti:");

        txtDeathDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        lblDateMessage.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblDateMessage.setText("Datum mora biti u formatu dd/MM/yyyy");

        javax.swing.GroupLayout pnlSpecificDeadLayout = new javax.swing.GroupLayout(pnlSpecificDead);
        pnlSpecificDead.setLayout(pnlSpecificDeadLayout);
        pnlSpecificDeadLayout.setHorizontalGroup(
            pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                        .addComponent(lblDeathDate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtDeathDate, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblDateMessage)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                        .addComponent(lblDeathPlace)
                        .addGap(20, 20, 20)
                        .addComponent(txtDeathPlace, javax.swing.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlSpecificDeadLayout.setVerticalGroup(
            pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDeathDate)
                    .addComponent(txtDeathDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDateMessage))
                .addGap(18, 18, 18)
                .addGroup(pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDeathPlace)
                    .addComponent(txtDeathPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
        );

        layerDeadAlive.setLayer(pnlSpecificAlive, javax.swing.JLayeredPane.DEFAULT_LAYER);
        layerDeadAlive.setLayer(pnlSpecificDead, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layerDeadAliveLayout = new javax.swing.GroupLayout(layerDeadAlive);
        layerDeadAlive.setLayout(layerDeadAliveLayout);
        layerDeadAliveLayout.setHorizontalGroup(
            layerDeadAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layerDeadAliveLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlSpecificAlive, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layerDeadAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layerDeadAliveLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(pnlSpecificDead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layerDeadAliveLayout.setVerticalGroup(
            layerDeadAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layerDeadAliveLayout.createSequentialGroup()
                .addComponent(pnlSpecificAlive, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layerDeadAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlSpecificDead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtName.setText("dummyn");

        txtSurname.setText("dummys");

        btnGroupGender.add(checkMale);
        checkMale.setText("Muški");

        btnGroupGender.add(checkFemale);
        checkFemale.setText("Ženski");

        txtBirthDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));
        txtBirthDate.setText("01/01/2020");

        lblMessage.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        lblMessage.setText("Datum mora biti u formatu: dd/MM/yyyy");

        javax.swing.GroupLayout pnlNewMemberDataLayout = new javax.swing.GroupLayout(pnlNewMemberData);
        pnlNewMemberData.setLayout(pnlNewMemberDataLayout);
        pnlNewMemberDataLayout.setHorizontalGroup(
            pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                        .addGap(124, 124, 124)
                        .addComponent(checkAlive)
                        .addGap(158, 158, 158)
                        .addComponent(checkDead)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(layerDeadAlive))
                    .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblBiography, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblSurname, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblGender, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblBirthDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblBirthPlace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPicture, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtName)
                            .addComponent(txtSurname, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtBirthPlace, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtUrlBiography, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtUrlPicture, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                        .addComponent(checkMale)
                                        .addGap(18, 18, 18)
                                        .addComponent(checkFemale))
                                    .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                        .addComponent(txtBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 68, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        pnlNewMemberDataLayout.setVerticalGroup(
            pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSurname)
                    .addComponent(txtSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblGender)
                    .addComponent(checkMale)
                    .addComponent(checkFemale))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBirthDate)
                    .addComponent(txtBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMessage))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBirthPlace)
                    .addComponent(txtBirthPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBiography)
                    .addComponent(txtUrlBiography, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPicture)
                    .addComponent(txtUrlPicture, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(checkAlive)
                    .addComponent(checkDead))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(layerDeadAlive, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );

        jLabel1.setText("Izaberite porodnično stablo u koje hoćete da unesete novog člana:");

        javax.swing.GroupLayout pnlFamilyTreeLayout = new javax.swing.GroupLayout(pnlFamilyTree);
        pnlFamilyTree.setLayout(pnlFamilyTreeLayout);
        pnlFamilyTreeLayout.setHorizontalGroup(
            pnlFamilyTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFamilyTreeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbFamilyTree, javax.swing.GroupLayout.PREFERRED_SIZE, 899, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlFamilyTreeLayout.setVerticalGroup(
            pnlFamilyTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFamilyTreeLayout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addGroup(pnlFamilyTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbFamilyTree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        layersAddFamilyMember.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Dodavanje člana porodice"));

        jLabel2.setText("Odaberite člana uže porodice:");

        btnAddFamilyMember.setText("Dodajte člana porodice");

        lblUloga.setText("Dodajte njegovu/njenu ulogu:");

        lblInstruction.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        lblInstruction.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInstruction.setText("Za definisanog člana morate uneti jednog roditelja ili jedno dete koje već postoji u stablu");

        javax.swing.GroupLayout pnlFamilyMembersLayout = new javax.swing.GroupLayout(pnlFamilyMembers);
        pnlFamilyMembers.setLayout(pnlFamilyMembersLayout);
        pnlFamilyMembersLayout.setHorizontalGroup(
            pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFamilyMembersLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlFamilyMembersLayout.createSequentialGroup()
                        .addComponent(lblInstruction, javax.swing.GroupLayout.PREFERRED_SIZE, 642, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlFamilyMembersLayout.createSequentialGroup()
                        .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblUloga, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbFamilyMember, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbRole, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(pnlFamilyMembersLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAddFamilyMember))))
        );
        pnlFamilyMembersLayout.setVerticalGroup(
            pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFamilyMembersLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblInstruction, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbFamilyMember, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlFamilyMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUloga)
                    .addComponent(cmbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAddFamilyMember)
                .addGap(6, 6, 6))
        );

        btnChangeFamilyMember.setText("Promenite člana porodice");

        lblAddedMember.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAddedMember.setText("Nije dodat ni jedan član porodice");

        javax.swing.GroupLayout pnlFamilyMemberAddedLayout = new javax.swing.GroupLayout(pnlFamilyMemberAdded);
        pnlFamilyMemberAdded.setLayout(pnlFamilyMemberAddedLayout);
        pnlFamilyMemberAddedLayout.setHorizontalGroup(
            pnlFamilyMemberAddedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFamilyMemberAddedLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFamilyMemberAddedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlFamilyMemberAddedLayout.createSequentialGroup()
                        .addGap(0, 417, Short.MAX_VALUE)
                        .addComponent(btnChangeFamilyMember, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblAddedMember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlFamilyMemberAddedLayout.setVerticalGroup(
            pnlFamilyMemberAddedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlFamilyMemberAddedLayout.createSequentialGroup()
                .addContainerGap(89, Short.MAX_VALUE)
                .addComponent(lblAddedMember)
                .addGap(18, 18, 18)
                .addComponent(btnChangeFamilyMember, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        layersAddFamilyMember.setLayer(pnlFamilyMembers, javax.swing.JLayeredPane.DEFAULT_LAYER);
        layersAddFamilyMember.setLayer(pnlFamilyMemberAdded, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layersAddFamilyMemberLayout = new javax.swing.GroupLayout(layersAddFamilyMember);
        layersAddFamilyMember.setLayout(layersAddFamilyMemberLayout);
        layersAddFamilyMemberLayout.setHorizontalGroup(
            layersAddFamilyMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layersAddFamilyMemberLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlFamilyMembers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(28, 28, 28))
            .addGroup(layersAddFamilyMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layersAddFamilyMemberLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(pnlFamilyMemberAdded, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(20, Short.MAX_VALUE)))
        );
        layersAddFamilyMemberLayout.setVerticalGroup(
            layersAddFamilyMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layersAddFamilyMemberLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlFamilyMembers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layersAddFamilyMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layersAddFamilyMemberLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(pnlFamilyMemberAdded, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout pnlSpouseLayout = new javax.swing.GroupLayout(pnlSpouse);
        pnlSpouse.setLayout(pnlSpouseLayout);
        pnlSpouseLayout.setHorizontalGroup(
            pnlSpouseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSpouseLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(spousePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlSpouseLayout.setVerticalGroup(
            pnlSpouseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpouseLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spousePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnAddMember.setFont(new java.awt.Font("Dialog", 1, 15)); // NOI18N
        btnAddMember.setText("Dodajte novog člana");

        javax.swing.GroupLayout pnlAddButtonLayout = new javax.swing.GroupLayout(pnlAddButton);
        pnlAddButton.setLayout(pnlAddButtonLayout);
        pnlAddButtonLayout.setHorizontalGroup(
            pnlAddButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnAddMember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlAddButtonLayout.setVerticalGroup(
            pnlAddButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAddButtonLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAddMember, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlFamilyTree, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pnlAddButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pnlNewMemberData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(layersAddFamilyMember)
                            .addComponent(pnlSpouse, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlFamilyTree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(layersAddFamilyMember, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlSpouse, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(pnlNewMemberData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    public JLayeredPane getLayerDeadAlive() {
        return layerDeadAlive;
    }

    public JButton getBtnAddFamilyMember() {
        return btnAddFamilyMember;
    }

    public JButton getBtnAddMember() {
        return btnAddMember;
    }

    public JButton getBtnChangeFamilyMember() {
        return btnChangeFamilyMember;
    }

    public ButtonGroup getBtnGroupGender() {
        return btnGroupGender;
    }

    public JCheckBox getCheckAlive() {
        return checkAlive;
    }

    public JCheckBox getCheckDead() {
        return checkDead;
    }

    public JCheckBox getCheckFemale() {
        return checkFemale;
    }

    public JCheckBox getCheckMale() {
        return checkMale;
    }

    public JComboBox<NodeDTO> getCmbFamilyMember() {
        return cmbFamilyMember;
    }

    public JComboBox<FamilyTreeDTO> getCmbFamilyTree() {
        return cmbFamilyTree;
    }

    public JComboBox<String> getCmbRole() {
        return cmbRole;
    }

    public JPanel getPnlFamilyMemberAdded() {
        return pnlFamilyMemberAdded;
    }

    public JPanel getPnlFamilyMembers() {
        return pnlFamilyMembers;
    }

    public JPanel getPnlFamilyTree() {
        return pnlFamilyTree;
    }

    public JPanel getPnlNewMemberData() {
        return pnlNewMemberData;
    }

    public JPanel getPnlSpecificAlive() {
        return pnlSpecificAlive;
    }

    public JPanel getPnlSpecificDead() {
        return pnlSpecificDead;
    }

    public SpousePanel getSpousePanel() {
        return spousePanel;
    }

    public JTextField getTxtAddress() {
        return txtAddress;
    }

    public JFormattedTextField getTxtBirthDate() {
        return txtBirthDate;
    }

    public JTextField getTxtBirthPlace() {
        return txtBirthPlace;
    }

    public JFormattedTextField getTxtDeathDate() {
        return txtDeathDate;
    }

    public JTextField getTxtDeathPlace() {
        return txtDeathPlace;
    }

    public JTextField getTxtName() {
        return txtName;
    }

    public JTextField getTxtSurname() {
        return txtSurname;
    }

    public JTextField getTxtUrlBiography() {
        return txtUrlBiography;
    }

    public JTextField getTxtUrlPicture() {
        return txtUrlPicture;
    }

    public ButtonGroup getGrpLivingStatus() {
        return grpLivingStatus;
    }

    public JLayeredPane getLayersAddFamilyMember() {
        return layersAddFamilyMember;
    }

    public JLabel getLblAddedMember() {
        return lblAddedMember;
    }

    public JPanel getPnlAddButton() {
        return pnlAddButton;
    }

    public JPanel getPnlSpouse() {
        return pnlSpouse;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddFamilyMember;
    private javax.swing.JButton btnAddMember;
    private javax.swing.JButton btnChangeFamilyMember;
    private javax.swing.ButtonGroup btnGroupGender;
    private javax.swing.JCheckBox checkAlive;
    private javax.swing.JCheckBox checkDead;
    private javax.swing.JCheckBox checkFemale;
    private javax.swing.JCheckBox checkMale;
    private javax.swing.JComboBox<NodeDTO> cmbFamilyMember;
    private javax.swing.JComboBox<FamilyTreeDTO> cmbFamilyTree;
    private javax.swing.JComboBox<String> cmbRole;
    private javax.swing.ButtonGroup grpLivingStatus;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLayeredPane layerDeadAlive;
    private javax.swing.JLayeredPane layersAddFamilyMember;
    private javax.swing.JLabel lblAddedMember;
    private javax.swing.JLabel lblAddress;
    private javax.swing.JLabel lblBiography;
    private javax.swing.JLabel lblBirthDate;
    private javax.swing.JLabel lblBirthPlace;
    private javax.swing.JLabel lblDateMessage;
    private javax.swing.JLabel lblDeathDate;
    private javax.swing.JLabel lblDeathPlace;
    private javax.swing.JLabel lblGender;
    private javax.swing.JLabel lblInstruction;
    private javax.swing.JLabel lblMessage;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPicture;
    private javax.swing.JLabel lblSurname;
    private javax.swing.JLabel lblUloga;
    private javax.swing.JPanel pnlAddButton;
    private javax.swing.JPanel pnlFamilyMemberAdded;
    private javax.swing.JPanel pnlFamilyMembers;
    private javax.swing.JPanel pnlFamilyTree;
    private javax.swing.JPanel pnlNewMemberData;
    private javax.swing.JPanel pnlSpecificAlive;
    private javax.swing.JPanel pnlSpecificDead;
    private javax.swing.JPanel pnlSpouse;
    private view.component.SpousePanel spousePanel;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JFormattedTextField txtBirthDate;
    private javax.swing.JTextField txtBirthPlace;
    private javax.swing.JFormattedTextField txtDeathDate;
    private javax.swing.JTextField txtDeathPlace;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtSurname;
    private javax.swing.JTextField txtUrlBiography;
    private javax.swing.JTextField txtUrlPicture;
    // End of variables declaration//GEN-END:variables

    private void prepareDialog() {
        this.setPreferredSize(pnlFamilyTree.getPreferredSize());
        pnlNewMemberData.setVisible(false);
        layersAddFamilyMember.setVisible(false);
        pnlAddButton.setVisible(false);
        pnlSpouse.setVisible(false);
    }

    public void printError(String err) {
        JOptionPane.showMessageDialog(this, err, "Greška", JOptionPane.ERROR_MESSAGE);
    }

    public void printInfo(String info) {
        JOptionPane.showMessageDialog(this, info, "Poruka", JOptionPane.INFORMATION_MESSAGE);
    }




}
