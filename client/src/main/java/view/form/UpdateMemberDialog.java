package view.form;

import javax.swing.*;

import dto.impl.FamilyTreeDTO;
import dto.impl.MemberInfoDTO;
import dto.impl.NodeUpdateDTO;
import model.impl.Node;
import model.enums.Gender;
import view.component.MemberInfoPanel;
import view.component.SpousePanel;

import java.util.Date;

public class UpdateMemberDialog extends javax.swing.JDialog {

    public UpdateMemberDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnUpdate = new javax.swing.JButton();
        cmbNodes = new javax.swing.JComboBox<>();
        lblChooseMember = new javax.swing.JLabel();
        memberInfoPanel = new view.component.MemberInfoPanel();
        spousePanel = new view.component.SpousePanel();
        jLabel1 = new javax.swing.JLabel();
        cmbFamilyTree = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Promena podataka o članu");

        btnUpdate.setText("Izmeni člana");

        lblChooseMember.setText("Odaberite člana koga želite da ažurirate:");

        jLabel1.setText("Odaberite rodoslov iz kog ćete odabrati člana za promenu:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(lblChooseMember)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(cmbNodes, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel1)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(cmbFamilyTree, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(memberInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(263, 263, 263)
                                                                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(spousePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(cmbFamilyTree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblChooseMember)
                                        .addComponent(cmbNodes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(memberInfoPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addContainerGap(126, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(spousePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(62, 62, 62))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    public JButton getBtnUpdate() {
        return btnUpdate;
    }

    public MemberInfoPanel getMemberInfoPanel() {
        return memberInfoPanel;
    }

    public SpousePanel getSpousePanel() {
        return spousePanel;
    }

    public JComboBox<NodeUpdateDTO> getCmbNodes() {
        return cmbNodes;
    }

    public JComboBox<FamilyTreeDTO> getCmbFamilyTree() {
        return cmbFamilyTree;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<FamilyTreeDTO> cmbFamilyTree;
    private javax.swing.JComboBox<NodeUpdateDTO> cmbNodes;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblChooseMember;
    private view.component.MemberInfoPanel memberInfoPanel;
    private view.component.SpousePanel spousePanel;
    // End of variables declaration//GEN-END:variables


    public void fillMemberInfo(MemberInfoDTO memberInfoDTO) {
        System.out.println(memberInfoDTO);
        /*obavezna polja*/
        memberInfoPanel.getTxtName().setText(memberInfoDTO.getName());
        memberInfoPanel.getTxtSurname().setText(memberInfoDTO.getSurname());
        if (memberInfoDTO.getGender().equals(Gender.MALE)) {
            memberInfoPanel.getCheckMale().setSelected(true);
        } else {
            memberInfoPanel.getCheckFemale().setSelected(true);
        }
        Date birth = java.sql.Date.valueOf(memberInfoDTO.getBirthDate());
        memberInfoPanel.getTxtBirthDate().setValue(birth);
        memberInfoPanel.getTxtBirthPlace().setText(memberInfoDTO.getBirthPlace());
        /*obavezna polja*/

        /*Opciona polja*/
        memberInfoPanel.getTxtUrlBiography().setText(memberInfoDTO.getBiographyUrl());
        memberInfoPanel.getTxtUrlPicture().setText(memberInfoDTO.getPictureUrl());
        /*Opciona polja*/


        /*Ako je član preminuo*/
        if (!memberInfoDTO.isAlive()) {
            Date deathDate = java.sql.Date.valueOf(memberInfoDTO.getDeathDate());
            memberInfoPanel.dead(deathDate, memberInfoDTO.getDeathPlace());
        } else  /*Ako je član živ*/
            memberInfoPanel.alive(memberInfoDTO.getAddress());

    }

    public void printError(String error) {
        JOptionPane.showMessageDialog(this, error, "Greška", JOptionPane.ERROR_MESSAGE);
    }

    public void printInfo(String info) {
        JOptionPane.showMessageDialog(this, info, "Poruka", JOptionPane.ERROR_MESSAGE);
    }
}
