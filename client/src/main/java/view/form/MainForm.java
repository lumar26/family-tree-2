package view.form;

import controller.UserController;
import dto.impl.UserDTO;

import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JMenuItem;

public class MainForm extends javax.swing.JFrame {


    public MainForm(UserDTO user) {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblLoggedUser = new javax.swing.JLabel();
        mainMenuBar = new javax.swing.JMenuBar();
        menuFamilyTree = new javax.swing.JMenu();
        optionCreateFamilyTree = new javax.swing.JMenuItem();
        optionDeleteFamilyTree = new javax.swing.JMenuItem();
        optionShowFamilyTree = new javax.swing.JMenuItem();
        menuMember = new javax.swing.JMenu();
        optionNewMember = new javax.swing.JMenuItem();
        optionChangeMember = new javax.swing.JMenuItem();
        optionDeleteMember = new javax.swing.JMenuItem();
        optionFindRelationship = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Vaše porodično stablo");

        lblLoggedUser.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        lblLoggedUser.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        menuFamilyTree.setText("Rodoslov");

        optionCreateFamilyTree.setText("Kreiraj novi rodoslov");
        menuFamilyTree.add(optionCreateFamilyTree);

        optionDeleteFamilyTree.setText("Obriši rodoslov");
        menuFamilyTree.add(optionDeleteFamilyTree);

        optionShowFamilyTree.setText("Prikaz rodoslova");
        menuFamilyTree.add(optionShowFamilyTree);

        mainMenuBar.add(menuFamilyTree);

        menuMember.setText("Član");

        optionNewMember.setText("Unos novog člana u rodoslov");
        menuMember.add(optionNewMember);

        optionChangeMember.setText("Izmeni člana");
        menuMember.add(optionChangeMember);

        optionDeleteMember.setText("Obriši člana ");
        menuMember.add(optionDeleteMember);

        optionFindRelationship.setText("Odredi rodbinsku vezu dva člana");
        menuMember.add(optionFindRelationship);

        mainMenuBar.add(menuMember);

        setJMenuBar(mainMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblLoggedUser, javax.swing.GroupLayout.DEFAULT_SIZE, 536, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addComponent(lblLoggedUser, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(81, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public JMenuItem getOptionChangeMember() {
        return optionChangeMember;
    }

    public JMenuItem getOptionCreateFamilyTree() {
        return optionCreateFamilyTree;
    }

    public JMenuItem getOptionDeleteFamilyTree() {
        return optionDeleteFamilyTree;
    }

    public JMenuItem getOptionDeleteMember() {
        return optionDeleteMember;
    }

    public JMenuItem getOptionNewMember() {
        return optionNewMember;
    }

    public JMenuItem getOptionShowFamilyTree() {
        return optionShowFamilyTree;
    }

    public JMenuItem getOptionFindRelationship() {
        return optionFindRelationship;
    }

    public JLabel getLblLoggedUser() {
        return lblLoggedUser;
    }


    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblLoggedUser;
    private javax.swing.JMenuBar mainMenuBar;
    private javax.swing.JMenu menuFamilyTree;
    private javax.swing.JMenu menuMember;
    private javax.swing.JMenuItem optionChangeMember;
    private javax.swing.JMenuItem optionCreateFamilyTree;
    private javax.swing.JMenuItem optionDeleteFamilyTree;
    private javax.swing.JMenuItem optionDeleteMember;
    private javax.swing.JMenuItem optionFindRelationship;
    private javax.swing.JMenuItem optionNewMember;
    private javax.swing.JMenuItem optionShowFamilyTree;
    // End of variables declaration//GEN-END:variables


    @Override
    public void dispose() {

        try {
            UserController.getInstance().logout();
        } catch (IOException e) {
            e.printStackTrace();
        }

        super.dispose();
        System.exit(0);
    }


}
