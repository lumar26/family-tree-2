package view.form;

import dto.impl.FamilyTreeDTO;
import dto.impl.NodeDTO;

import javax.swing.*;
import java.awt.*;

public class DeleteMemberDialog extends javax.swing.JDialog {
    
    
    public DeleteMemberDialog(Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlChooseTree = new javax.swing.JPanel();
        lblChooseTree = new javax.swing.JLabel();
        cmbFamilyTree = new javax.swing.JComboBox<>();
        pnlChooseMember = new javax.swing.JPanel();
        lblChooseNode = new javax.swing.JLabel();
        cmbNodes = new javax.swing.JComboBox<>();
        btnDeleteNode = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Brisanje člana");

        lblChooseTree.setText("Izaberite stablo iz kog želite da obrišete člana:");

        javax.swing.GroupLayout pnlChooseTreeLayout = new javax.swing.GroupLayout(pnlChooseTree);
        pnlChooseTree.setLayout(pnlChooseTreeLayout);
        pnlChooseTreeLayout.setHorizontalGroup(
            pnlChooseTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlChooseTreeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblChooseTree)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbFamilyTree, 0, 358, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlChooseTreeLayout.setVerticalGroup(
            pnlChooseTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlChooseTreeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlChooseTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblChooseTree)
                    .addComponent(cmbFamilyTree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblChooseNode.setText("Izaberite člana stabla za brisanje:");

        javax.swing.GroupLayout pnlChooseMemberLayout = new javax.swing.GroupLayout(pnlChooseMember);
        pnlChooseMember.setLayout(pnlChooseMemberLayout);
        pnlChooseMemberLayout.setHorizontalGroup(
            pnlChooseMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlChooseMemberLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblChooseNode)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbNodes, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlChooseMemberLayout.setVerticalGroup(
            pnlChooseMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlChooseMemberLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlChooseMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblChooseNode)
                    .addComponent(cmbNodes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnDeleteNode.setText("Obriši");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlChooseTree, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlChooseMember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDeleteNode, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlChooseTree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlChooseMember, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDeleteNode)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public JButton getBtnDeleteNode() {
        return btnDeleteNode;
    }

    public JComboBox<FamilyTreeDTO> getCmbFamilyTree() {
        return cmbFamilyTree;
    }

    public JComboBox<NodeDTO> getCmbNodes() {
        return cmbNodes;
    }

    public JPanel getPnlChooseMember() {
        return pnlChooseMember;
    }

    public JPanel getPnlChooseTree() {
        return pnlChooseTree;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDeleteNode;
    private javax.swing.JComboBox<FamilyTreeDTO> cmbFamilyTree;
    private javax.swing.JComboBox<NodeDTO> cmbNodes;
    private javax.swing.JLabel lblChooseNode;
    private javax.swing.JLabel lblChooseTree;
    private javax.swing.JPanel pnlChooseMember;
    private javax.swing.JPanel pnlChooseTree;
    // End of variables declaration//GEN-END:variables

    public void printError(String error){
        JOptionPane.showMessageDialog(this, error, "Greška", JOptionPane.ERROR_MESSAGE);
    }

    public void printInfo(String info){
        JOptionPane.showMessageDialog(this, info, "Poruka", JOptionPane.INFORMATION_MESSAGE);
    }
}
