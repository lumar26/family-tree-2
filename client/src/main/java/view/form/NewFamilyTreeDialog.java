package view.form;

import javax.swing.*;
import java.awt.*;

public class NewFamilyTreeDialog extends javax.swing.JDialog {

    public NewFamilyTreeDialog(Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblDialogDescription = new javax.swing.JLabel();
        pnlInsertInfo = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblDescription = new javax.swing.JLabel();
        lblStartDate = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtDescription = new javax.swing.JTextField();
        dateStartDate = new javax.swing.JFormattedTextField();
        lblInform = new javax.swing.JLabel();
        btnSaveFamilyTree = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Novi rodoslov");

        lblDialogDescription.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDialogDescription.setText("Unos novog rodoslova u sistem");

        pnlInsertInfo.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Unos opštih informacija o rodoslovu"));

        lblName.setText("Naziv vašeg rodoslova:");

        lblDescription.setText("Kratak opis Vašeg rodoslova:");

        lblStartDate.setText("Datum početka posmatranja rodoslova:");

        txtName.setText("dummyTree");

        txtDescription.setText("dummyDescription");

        dateStartDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));
        dateStartDate.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        dateStartDate.setText("01/01/2020");

        lblInform.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblInform.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInform.setText(" format dd/MM/yyyy je obavezan");

        javax.swing.GroupLayout pnlInsertInfoLayout = new javax.swing.GroupLayout(pnlInsertInfo);
        pnlInsertInfo.setLayout(pnlInsertInfoLayout);
        pnlInsertInfoLayout.setHorizontalGroup(
            pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInsertInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblStartDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDescription, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtName)
                    .addComponent(txtDescription)
                    .addGroup(pnlInsertInfoLayout.createSequentialGroup()
                        .addComponent(dateStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblInform, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlInsertInfoLayout.setVerticalGroup(
            pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInsertInfoLayout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addGroup(pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlInsertInfoLayout.createSequentialGroup()
                        .addGroup(pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblName)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(50, 50, 50)
                        .addComponent(lblDescription))
                    .addComponent(txtDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(pnlInsertInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStartDate)
                    .addComponent(dateStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblInform))
                .addGap(46, 46, 46))
        );

        btnSaveFamilyTree.setText("Sačuvaj rodoslov");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSaveFamilyTree))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDialogDescription, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlInsertInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblDialogDescription)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlInsertInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSaveFamilyTree)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    public JButton getBtnSaveFamilyTree() {
        return btnSaveFamilyTree;
    }

    public JFormattedTextField getDateStartDate() {
        return dateStartDate;
    }

    public JPanel getPnlInsertInfo() {
        return pnlInsertInfo;
    }

    public JTextField getTxtDescription() {
        return txtDescription;
    }

    public JTextField getTxtName() {
        return txtName;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSaveFamilyTree;
    private javax.swing.JFormattedTextField dateStartDate;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblDialogDescription;
    private javax.swing.JLabel lblInform;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblStartDate;
    private javax.swing.JPanel pnlInsertInfo;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables

    public void printError(Exception e) {
        JOptionPane.showMessageDialog(this, e.getMessage(), "Greška", JOptionPane.ERROR_MESSAGE);
    }

    public void printInfo(String info) {
        JOptionPane.showMessageDialog(this, info, "Greška", JOptionPane.INFORMATION_MESSAGE);
    }
}
