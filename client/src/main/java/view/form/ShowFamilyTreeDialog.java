
package view.form;

import dto.impl.FamilyTreeDTO;

import javax.swing.*;

import view.component.FamilyTreePanel;

public class ShowFamilyTreeDialog extends javax.swing.JDialog {

    public ShowFamilyTreeDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblChooseFamilyTree = new javax.swing.JLabel();
        cmbFamilyTree = new javax.swing.JComboBox<>();
        btnShowFamilyTree = new javax.swing.JButton();
        treePanel = new view.component.FamilyTreePanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lblChooseFamilyTree.setText("Odaberite rodoslov za prikaz:");

        btnShowFamilyTree.setText("Prikaži");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(treePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblChooseFamilyTree)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnShowFamilyTree, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbFamilyTree, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblChooseFamilyTree)
                    .addComponent(cmbFamilyTree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnShowFamilyTree)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(treePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 406, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public JButton getBtnShowFamilyTree() {
        return btnShowFamilyTree;
    }

    public JComboBox<FamilyTreeDTO> getCmbFamilyTree() {
        return cmbFamilyTree;
    }

    public FamilyTreePanel getTreePanel() {
        return treePanel;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnShowFamilyTree;
    private javax.swing.JComboBox<FamilyTreeDTO> cmbFamilyTree;
    private javax.swing.JLabel lblChooseFamilyTree;
    private view.component.FamilyTreePanel treePanel;
    // End of variables declaration//GEN-END:variables


    public void printError(String error){
        JOptionPane.showMessageDialog(this, error, "Greška", JOptionPane.ERROR_MESSAGE);
    }

    public void printInfo(String info){
        JOptionPane.showMessageDialog(this, info, "Poruka", JOptionPane.INFORMATION_MESSAGE);
    }

}
