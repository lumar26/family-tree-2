package view.form;

import dto.impl.FamilyTreeDTO;

import javax.swing.*;

public class DeleteFamilyTreeDialog extends javax.swing.JDialog {


    public DeleteFamilyTreeDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlDelete = new javax.swing.JPanel();
        lblDelete = new javax.swing.JLabel();
        cmbFamilyTrees = new javax.swing.JComboBox<>();
        btnDeleteFamilyTree = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Brisanje rodoslova");

        lblDelete.setText("Izaberite rodoslov koji želite da obrišete:");

        btnDeleteFamilyTree.setText("Obriši");

        javax.swing.GroupLayout pnlDeleteLayout = new javax.swing.GroupLayout(pnlDelete);
        pnlDelete.setLayout(pnlDeleteLayout);
        pnlDeleteLayout.setHorizontalGroup(
                pnlDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlDeleteLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(pnlDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(pnlDeleteLayout.createSequentialGroup()
                                                .addComponent(lblDelete)
                                                .addGap(0, 186, Short.MAX_VALUE))
                                        .addComponent(cmbFamilyTrees, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDeleteLayout.createSequentialGroup()
                                                .addGap(0, 0, Short.MAX_VALUE)
                                                .addComponent(btnDeleteFamilyTree)))
                                .addContainerGap())
        );
        pnlDeleteLayout.setVerticalGroup(
                pnlDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlDeleteLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(lblDelete)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cmbFamilyTrees, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnDeleteFamilyTree)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(pnlDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    public JButton getBtnDeleteFamilyTree() {
        return btnDeleteFamilyTree;
    }

    public JComboBox<FamilyTreeDTO> getCmbFamilyTrees() {
        return cmbFamilyTrees;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDeleteFamilyTree;
    private javax.swing.JComboBox<FamilyTreeDTO> cmbFamilyTrees;
    private javax.swing.JLabel lblDelete;
    private javax.swing.JPanel pnlDelete;
    // End of variables declaration//GEN-END:variables


    public void printError(String error) {
        JOptionPane.showMessageDialog(this, error, "Greška", JOptionPane.ERROR_MESSAGE);
    }

    public void printMessage(String msg) {
        JOptionPane.showMessageDialog(this, msg, "Poruka", JOptionPane.INFORMATION_MESSAGE);
    }
}
