package view.form;

import dto.impl.FamilyTreeDTO;
import dto.impl.NodeDTO;

import javax.swing.*;

public class FindRelationshipDialog extends javax.swing.JDialog {

    public FindRelationshipDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlFamilyTree = new javax.swing.JPanel();
        lblChooseTree = new javax.swing.JLabel();
        cmbFamilyTrees = new javax.swing.JComboBox<>();
        lblMember1 = new javax.swing.JLabel();
        cmbMember2 = new javax.swing.JComboBox<>();
        lblMember2 = new javax.swing.JLabel();
        cmbMember1 = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtRelationship = new javax.swing.JTextArea();
        btnFindRelationship = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lblChooseTree.setText("Odaberite stablo iz kog ćete odabrati dva člana za određivanje njihovog srodstva:");

        javax.swing.GroupLayout pnlFamilyTreeLayout = new javax.swing.GroupLayout(pnlFamilyTree);
        pnlFamilyTree.setLayout(pnlFamilyTreeLayout);
        pnlFamilyTreeLayout.setHorizontalGroup(
            pnlFamilyTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFamilyTreeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFamilyTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbFamilyTrees, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblChooseTree, javax.swing.GroupLayout.DEFAULT_SIZE, 947, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlFamilyTreeLayout.setVerticalGroup(
            pnlFamilyTreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFamilyTreeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblChooseTree)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbFamilyTrees, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblMember1.setText("Odaberite prvog člana:");

        lblMember2.setText("Odaberite drugog člana:");

        txtRelationship.setColumns(20);
        txtRelationship.setFont(new java.awt.Font("DejaVu Serif", 2, 14)); // NOI18N
        txtRelationship.setRows(5);
        jScrollPane1.setViewportView(txtRelationship);

        btnFindRelationship.setText("Odredi srodstvo");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlFamilyTree, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnFindRelationship, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblMember1)
                            .addComponent(cmbMember1, javax.swing.GroupLayout.PREFERRED_SIZE, 459, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbMember2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblMember2)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlFamilyTree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMember1)
                    .addComponent(lblMember2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbMember2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbMember1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnFindRelationship, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public JButton getBtnFindRelationship() {
        return btnFindRelationship;
    }

    public JComboBox<FamilyTreeDTO> getCmbFamilyTrees() {
        return cmbFamilyTrees;
    }

    public JComboBox<NodeDTO> getCmbMember1() {
        return cmbMember1;
    }

    public JComboBox<NodeDTO> getCmbMember2() {
        return cmbMember2;
    }


    
    public JTextArea getTxtRelationship() {
        return txtRelationship;
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFindRelationship;
    private javax.swing.JComboBox<FamilyTreeDTO> cmbFamilyTrees;
    private javax.swing.JComboBox<NodeDTO> cmbMember1;
    private javax.swing.JComboBox<NodeDTO> cmbMember2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblChooseTree;
    private javax.swing.JLabel lblMember1;
    private javax.swing.JLabel lblMember2;
    private javax.swing.JPanel pnlFamilyTree;
    private javax.swing.JTextArea txtRelationship;
    // End of variables declaration//GEN-END:variables

    public void printError(String error) {
        JOptionPane.showMessageDialog(this, error, "Greška", JOptionPane.ERROR_MESSAGE);
    }

    public void printInfo(String info) {
        JOptionPane.showMessageDialog(this, info, "Poruka", JOptionPane.INFORMATION_MESSAGE);
    }

    public void fillMemberCombos(NodeDTO[] membersOfTree) {
        cmbMember1.setModel(new DefaultComboBoxModel<>(membersOfTree));
        cmbMember1.setSelectedIndex(-1);
        cmbMember2.setModel(new DefaultComboBoxModel<>(membersOfTree));
        cmbMember2.setSelectedIndex(-1);
    }
}
