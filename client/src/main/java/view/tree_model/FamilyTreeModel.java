package view.tree_model;

import dto.impl.TreeNodeDTO;
import model.impl.Node;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;

public class FamilyTreeModel implements TreeModel {

    private final TreeNodeDTO root;
    private final ArrayList<TreeModelListener> treeModelListeners = new ArrayList<>();

    public FamilyTreeModel(TreeNodeDTO root) {
        this.root = root;
    }

    protected void fireTreeStructureChanged(Node oldRoot) {
        TreeModelEvent e = new TreeModelEvent(this,
                new Object[]{oldRoot});
        for (TreeModelListener tml : treeModelListeners) {
            tml.treeStructureChanged(e);
        }
    }

    @Override
    public Object getRoot() {
        return root;
    }

    @Override
    public Object getChild(Object o, int i) {
        TreeNodeDTO node = (TreeNodeDTO) o;
        if (i >= 0 && i < node.getChildren().size())
            return node.getChild(i);
        return null;
    }

    @Override
    public int getChildCount(Object o) {
        return o == null || ((TreeNodeDTO) o).getChildren() == null ? 0 : ((TreeNodeDTO) o).getChildCount();
    }

    @Override
    public boolean isLeaf(Object o) {
        TreeNodeDTO node = (TreeNodeDTO) o;
        return !node.hasChildren();
    }

    @Override
    public void valueForPathChanged(TreePath treePath, Object o) {
        System.out.println("*** valueForPathChanged : "
                + treePath + " --> " + o);
    }

    @Override
    public int getIndexOfChild(Object p, Object c) {
        if (p == null || c == null) return -1;

        TreeNodeDTO parent = (TreeNodeDTO) p;
        TreeNodeDTO child = (TreeNodeDTO) c;
        /*ovde bi eventualno mogla da se sortiraju deca*/
        return parent.getIndexOfChild(child);
    }

    @Override
    public void addTreeModelListener(TreeModelListener treeModelListener) {
        treeModelListeners.add(treeModelListener);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener treeModelListener) {
        treeModelListeners.remove(treeModelListener);
    }
}
