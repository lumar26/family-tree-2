package view.component;

import javax.swing.*;
import java.util.Date;

public class MemberInfoPanel extends javax.swing.JPanel {

    public MemberInfoPanel() {
        initComponents();
        preparePanel();
    }

    private void preparePanel() {
        layerDeadAlive.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupGender = new javax.swing.ButtonGroup();
        buttonGroupLifeStatus = new javax.swing.ButtonGroup();
        pnlNewMemberData = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblSurname = new javax.swing.JLabel();
        lblGender = new javax.swing.JLabel();
        lblBirthDate = new javax.swing.JLabel();
        lblBirthPlace = new javax.swing.JLabel();
        lblBiography = new javax.swing.JLabel();
        lblPicture = new javax.swing.JLabel();
        checkAlive = new javax.swing.JCheckBox();
        checkDead = new javax.swing.JCheckBox();
        layerDeadAlive = new javax.swing.JLayeredPane();
        pnlSpecificAlive = new javax.swing.JPanel();
        lblAddress = new javax.swing.JLabel();
        txtAddress = new javax.swing.JTextField();
        pnlSpecificDead = new javax.swing.JPanel();
        lblDeathDate = new javax.swing.JLabel();
        lblDeathPlace = new javax.swing.JLabel();
        txtDeathPlace = new javax.swing.JTextField();
        txtDeathDate = new javax.swing.JFormattedTextField();
        lblDateMessage = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtSurname = new javax.swing.JTextField();
        txtBirthPlace = new javax.swing.JTextField();
        txtUrlBiography = new javax.swing.JTextField();
        txtUrlPicture = new javax.swing.JTextField();
        checkMale = new javax.swing.JCheckBox();
        checkFemale = new javax.swing.JCheckBox();
        txtBirthDate = new javax.swing.JFormattedTextField();
        lblInstruction = new javax.swing.JLabel();

        pnlNewMemberData.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Podaci o članu"));

        lblName.setText("Ime člana:");

        lblSurname.setText("Prezime člana:");

        lblGender.setText("Pol:");

        lblBirthDate.setText("Datum rođenja:");

        lblBirthPlace.setText("Mesto rođenja:");

        lblBiography.setText("URL ka biografiji:");

        lblPicture.setText("URL ka slici:");

        buttonGroupLifeStatus.add(checkAlive);
        checkAlive.setText("Član je živ");
        checkAlive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkAliveActionPerformed(evt);
            }
        });

        buttonGroupLifeStatus.add(checkDead);
        checkDead.setText("Član je preminuo");
        checkDead.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkDeadActionPerformed(evt);
            }
        });

        lblAddress.setText("Trenutna adresa člana:");

        javax.swing.GroupLayout pnlSpecificAliveLayout = new javax.swing.GroupLayout(pnlSpecificAlive);
        pnlSpecificAlive.setLayout(pnlSpecificAliveLayout);
        pnlSpecificAliveLayout.setHorizontalGroup(
            pnlSpecificAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpecificAliveLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(lblAddress)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAddress)
                .addContainerGap())
        );
        pnlSpecificAliveLayout.setVerticalGroup(
            pnlSpecificAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpecificAliveLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSpecificAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAddress)
                    .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(70, Short.MAX_VALUE))
        );

        lblDeathDate.setText("Datum smrti:");

        lblDeathPlace.setText("Mesto smrti:");

        txtDeathDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        lblDateMessage.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        lblDateMessage.setText("Datum mora biti u formatu dd/MM/yyyy");

        javax.swing.GroupLayout pnlSpecificDeadLayout = new javax.swing.GroupLayout(pnlSpecificDead);
        pnlSpecificDead.setLayout(pnlSpecificDeadLayout);
        pnlSpecificDeadLayout.setHorizontalGroup(
            pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                        .addComponent(lblDeathDate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtDeathDate, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblDateMessage)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                        .addComponent(lblDeathPlace)
                        .addGap(20, 20, 20)
                        .addComponent(txtDeathPlace, javax.swing.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlSpecificDeadLayout.setVerticalGroup(
            pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpecificDeadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDeathDate)
                    .addComponent(txtDeathDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDateMessage))
                .addGap(18, 18, 18)
                .addGroup(pnlSpecificDeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDeathPlace)
                    .addComponent(txtDeathPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(45, Short.MAX_VALUE))
        );

        layerDeadAlive.setLayer(pnlSpecificAlive, javax.swing.JLayeredPane.DEFAULT_LAYER);
        layerDeadAlive.setLayer(pnlSpecificDead, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layerDeadAliveLayout = new javax.swing.GroupLayout(layerDeadAlive);
        layerDeadAlive.setLayout(layerDeadAliveLayout);
        layerDeadAliveLayout.setHorizontalGroup(
            layerDeadAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layerDeadAliveLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlSpecificAlive, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layerDeadAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layerDeadAliveLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(pnlSpecificDead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layerDeadAliveLayout.setVerticalGroup(
            layerDeadAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layerDeadAliveLayout.createSequentialGroup()
                .addComponent(pnlSpecificAlive, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layerDeadAliveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlSpecificDead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonGroupGender.add(checkMale);
        checkMale.setText("Muški");
        checkMale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkMaleActionPerformed(evt);
            }
        });

        buttonGroupGender.add(checkFemale);
        checkFemale.setText("Ženski");
        checkFemale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkFemaleActionPerformed(evt);
            }
        });

        txtBirthDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        lblInstruction.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        lblInstruction.setText("Datum mora biti u formatu: dd/MM/yyyy ");

        javax.swing.GroupLayout pnlNewMemberDataLayout = new javax.swing.GroupLayout(pnlNewMemberData);
        pnlNewMemberData.setLayout(pnlNewMemberDataLayout);
        pnlNewMemberDataLayout.setHorizontalGroup(
            pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                        .addGap(124, 124, 124)
                        .addComponent(checkAlive)
                        .addGap(158, 158, 158)
                        .addComponent(checkDead)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(layerDeadAlive))
                    .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblBiography, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblSurname, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblGender, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblBirthDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblBirthPlace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblPicture, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtName)
                            .addComponent(txtSurname, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtBirthPlace, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtUrlBiography, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtUrlPicture, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                        .addComponent(checkMale)
                                        .addGap(18, 18, 18)
                                        .addComponent(checkFemale))
                                    .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                                        .addComponent(txtBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblInstruction, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 68, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        pnlNewMemberDataLayout.setVerticalGroup(
            pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlNewMemberDataLayout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSurname)
                    .addComponent(txtSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblGender)
                    .addComponent(checkMale)
                    .addComponent(checkFemale))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBirthDate)
                    .addComponent(txtBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblInstruction))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBirthPlace)
                    .addComponent(txtBirthPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBiography)
                    .addComponent(txtUrlBiography, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPicture)
                    .addComponent(txtUrlPicture, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pnlNewMemberDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(checkAlive)
                    .addComponent(checkDead))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(layerDeadAlive, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlNewMemberData, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlNewMemberData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void checkAliveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkAliveActionPerformed
        layerDeadAlive.setVisible(true);
        pnlSpecificAlive.setVisible(true);
        pnlSpecificDead.setVisible(false);
    }//GEN-LAST:event_checkAliveActionPerformed

    private void checkDeadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkDeadActionPerformed
        layerDeadAlive.setVisible(true);
        pnlSpecificAlive.setVisible(false);
        pnlSpecificDead.setVisible(true);
    }//GEN-LAST:event_checkDeadActionPerformed

    private void checkMaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkMaleActionPerformed
    }//GEN-LAST:event_checkMaleActionPerformed

    private void checkFemaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkFemaleActionPerformed
    }//GEN-LAST:event_checkFemaleActionPerformed

    public JCheckBox getCheckAlive() {
        return checkAlive;
    }

    public JCheckBox getCheckDead() {
        return checkDead;
    }

    public JCheckBox getCheckFemale() {
        return checkFemale;
    }

    public JCheckBox getCheckMale() {
        return checkMale;
    }

    public JTextField getTxtAddress() {
        return txtAddress;
    }

    public JFormattedTextField getTxtBirthDate() {
        return txtBirthDate;
    }

    public JTextField getTxtBirthPlace() {
        return txtBirthPlace;
    }

    public JFormattedTextField getTxtDeathDate() {
        return txtDeathDate;
    }

    public JTextField getTxtDeathPlace() {
        return txtDeathPlace;
    }

    public JTextField getTxtName() {
        return txtName;
    }

    public JTextField getTxtSurname() {
        return txtSurname;
    }

    public JTextField getTxtUrlBiography() {
        return txtUrlBiography;
    }

    public JTextField getTxtUrlPicture() {
        return txtUrlPicture;
    }

    public JLayeredPane getLayerDeadAlive() {
        return layerDeadAlive;
    }

    public JPanel getPnlSpecificAlive() {
        return pnlSpecificAlive;
    }

    public JPanel getPnlSpecificDead() {
        return pnlSpecificDead;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupGender;
    private javax.swing.ButtonGroup buttonGroupLifeStatus;
    private javax.swing.JCheckBox checkAlive;
    private javax.swing.JCheckBox checkDead;
    private javax.swing.JCheckBox checkFemale;
    private javax.swing.JCheckBox checkMale;
    private javax.swing.JLayeredPane layerDeadAlive;
    private javax.swing.JLabel lblAddress;
    private javax.swing.JLabel lblBiography;
    private javax.swing.JLabel lblBirthDate;
    private javax.swing.JLabel lblBirthPlace;
    private javax.swing.JLabel lblDateMessage;
    private javax.swing.JLabel lblDeathDate;
    private javax.swing.JLabel lblDeathPlace;
    private javax.swing.JLabel lblGender;
    private javax.swing.JLabel lblInstruction;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPicture;
    private javax.swing.JLabel lblSurname;
    private javax.swing.JPanel pnlNewMemberData;
    private javax.swing.JPanel pnlSpecificAlive;
    private javax.swing.JPanel pnlSpecificDead;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JFormattedTextField txtBirthDate;
    private javax.swing.JTextField txtBirthPlace;
    private javax.swing.JFormattedTextField txtDeathDate;
    private javax.swing.JTextField txtDeathPlace;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtSurname;
    private javax.swing.JTextField txtUrlBiography;
    private javax.swing.JTextField txtUrlPicture;
    // End of variables declaration//GEN-END:variables

    public void alive(String address){
        txtAddress.setText(address);
        txtDeathDate.setText("N/A");
        txtDeathPlace.setText("N/A");

        checkAlive.setSelected(true);
        layerDeadAlive.setVisible(true);
        pnlSpecificAlive.setVisible(true);
        pnlSpecificDead.setVisible(false);
    }

    public void dead(Date deathDate, String deathPlace){
        txtDeathDate.setValue(deathDate);
        txtDeathPlace.setText(deathPlace == null ? "N/A" : deathPlace);
        txtAddress.setText("N/A");

        checkDead.setSelected(true);
        layerDeadAlive.setVisible(true);
        pnlSpecificAlive.setVisible(false);
        pnlSpecificDead.setVisible(true);
    }
}
