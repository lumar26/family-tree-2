package view.component;

import javax.swing.JScrollPane;
import javax.swing.JTree;

public class FamilyTreePanel extends javax.swing.JPanel {

    public FamilyTreePanel() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrolTree = new javax.swing.JScrollPane();
        treeFamily = new javax.swing.JTree();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Rodoslov"));

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("root");
        treeFamily.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        scrolTree.setViewportView(treeFamily);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrolTree, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrolTree, javax.swing.GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    public JScrollPane getScrolTree() {
        return scrolTree;
    }

    public JTree getTreeFamily() {
        return treeFamily;
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane scrolTree;
    private javax.swing.JTree treeFamily;
    // End of variables declaration//GEN-END:variables
}
