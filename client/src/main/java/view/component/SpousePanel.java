package view.component;

import java.awt.event.ItemEvent;
import javax.swing.*;

import controller.NodeController;
import dto.impl.FamilyTreeDTO;
import dto.impl.NodeDTO;

public class SpousePanel extends javax.swing.JPanel {

    private NodeController nodeController;

    private FamilyTreeDTO[] trees;
    private FamilyTreeDTO selectedTree;
    private NodeDTO selectedSpouse;

    public SpousePanel() {
        initComponents();
        nodeController = NodeController.getInstance();
        setVisible(true);

        preparePanel();
    }

    public FamilyTreeDTO[] getTrees() {
        return trees;
    }

    public void setTrees(FamilyTreeDTO[] trees) {
        this.trees = trees;
        populateCombos();
    }

    public NodeDTO getSelectedSpouse() {
        return selectedSpouse;
    }

    public JPanel getPnlAddSpouse() {
        return pnlAddSpouse;
    }

    public JPanel getPnlSpouseAdded() {
        return pnlSpouseAdded;
    }

    public JLabel getLblSpouseAdded() {
        return lblSpouseAdded;
    }

    public JComboBox<NodeDTO> getCmbFemaleMembers() {
        return cmbFemaleMembers;
    }

    public JComboBox<FamilyTreeDTO> getCmbFamilyTrees() {
        return cmbFamilyTrees;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        layeredPane = new javax.swing.JLayeredPane();
        pnlAddSpouse = new javax.swing.JPanel();
        lblMessage = new javax.swing.JLabel();
        cmbFamilyTrees = new javax.swing.JComboBox<>();
        cmbFemaleMembers = new javax.swing.JComboBox<>();
        btnSaveSpouse = new javax.swing.JButton();
        lblChooseTree = new javax.swing.JLabel();
        lblChooseMember = new javax.swing.JLabel();
        lblInstruction = new javax.swing.JLabel();
        pnlSpouseAdded = new javax.swing.JPanel();
        lblSpouseAdded = new javax.swing.JLabel();
        btnChangeSpouse = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Podaci o supružniku"));

        lblMessage.setText("Dodajte suprugu za člana koga ste do sada definisali:");

        cmbFamilyTrees.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbFamilyTreesItemStateChanged(evt);
            }
        });

        cmbFemaleMembers.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbFemaleMembersItemStateChanged(evt);
            }
        });

        btnSaveSpouse.setText("Sačuvajte suprugu");
        btnSaveSpouse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveSpouseActionPerformed(evt);
            }
        });

        lblChooseTree.setText("Odaberite stablo:");

        lblChooseMember.setText("Odaberite člana:");

        lblInstruction.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        lblInstruction.setText(" ovaj korak je opcioni  ");

        javax.swing.GroupLayout pnlAddSpouseLayout = new javax.swing.GroupLayout(pnlAddSpouse);
        pnlAddSpouse.setLayout(pnlAddSpouseLayout);
        pnlAddSpouseLayout.setHorizontalGroup(
            pnlAddSpouseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAddSpouseLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAddSpouseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlAddSpouseLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSaveSpouse, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlAddSpouseLayout.createSequentialGroup()
                        .addGroup(pnlAddSpouseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblChooseTree, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                            .addComponent(lblChooseMember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlAddSpouseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbFamilyTrees, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbFemaleMembers, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(pnlAddSpouseLayout.createSequentialGroup()
                        .addComponent(lblMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 393, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 108, Short.MAX_VALUE)
                        .addComponent(lblInstruction)
                        .addGap(29, 29, 29)))
                .addContainerGap())
        );
        pnlAddSpouseLayout.setVerticalGroup(
            pnlAddSpouseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAddSpouseLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlAddSpouseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMessage)
                    .addComponent(lblInstruction))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlAddSpouseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbFamilyTrees, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblChooseTree))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlAddSpouseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbFemaleMembers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblChooseMember))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSaveSpouse)
                .addContainerGap())
        );

        btnChangeSpouse.setText("Promenite suprugu novog člana");
        btnChangeSpouse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeSpouseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlSpouseAddedLayout = new javax.swing.GroupLayout(pnlSpouseAdded);
        pnlSpouseAdded.setLayout(pnlSpouseAddedLayout);
        pnlSpouseAddedLayout.setHorizontalGroup(
            pnlSpouseAddedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpouseAddedLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(pnlSpouseAddedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblSpouseAdded, javax.swing.GroupLayout.PREFERRED_SIZE, 618, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChangeSpouse, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        pnlSpouseAddedLayout.setVerticalGroup(
            pnlSpouseAddedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpouseAddedLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(lblSpouseAdded, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnChangeSpouse)
                .addContainerGap(34, Short.MAX_VALUE))
        );

        layeredPane.setLayer(pnlAddSpouse, javax.swing.JLayeredPane.DEFAULT_LAYER);
        layeredPane.setLayer(pnlSpouseAdded, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout layeredPaneLayout = new javax.swing.GroupLayout(layeredPane);
        layeredPane.setLayout(layeredPaneLayout);
        layeredPaneLayout.setHorizontalGroup(
            layeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlAddSpouse, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlSpouseAdded, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layeredPaneLayout.setVerticalGroup(
            layeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layeredPaneLayout.createSequentialGroup()
                .addComponent(pnlAddSpouse, javax.swing.GroupLayout.PREFERRED_SIZE, 141, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layeredPaneLayout.createSequentialGroup()
                    .addComponent(pnlSpouseAdded, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layeredPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layeredPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnChangeSpouseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeSpouseActionPerformed
        pnlAddSpouse.setVisible(true);
        pnlSpouseAdded.setVisible(false);
    }//GEN-LAST:event_btnChangeSpouseActionPerformed

    private void btnSaveSpouseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveSpouseActionPerformed
        if (selectedSpouse == null) {
            JOptionPane.showMessageDialog(this, "Morate izabrati suprugu iz padajuće liste", "Greška", JOptionPane.ERROR_MESSAGE);
            return;
        }
        pnlAddSpouse.setVisible(false);
        pnlSpouseAdded.setVisible(true);
    }//GEN-LAST:event_btnSaveSpouseActionPerformed

    private void cmbFamilyTreesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbFamilyTreesItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            this.selectedTree = (FamilyTreeDTO) cmbFamilyTrees.getSelectedItem();
            try {
                NodeDTO[] spouses = nodeController.getNodesOfTree(this.selectedTree);
                cmbFemaleMembers.setModel(new DefaultComboBoxModel<>(spouses));
                cmbFemaleMembers.setSelectedIndex(-1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_cmbFamilyTreesItemStateChanged

    private void cmbFemaleMembersItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbFemaleMembersItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            this.selectedSpouse = (NodeDTO) cmbFemaleMembers.getSelectedItem();
            assert selectedSpouse != null;
        }
    }//GEN-LAST:event_cmbFemaleMembersItemStateChanged



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChangeSpouse;
    private javax.swing.JButton btnSaveSpouse;
    private javax.swing.JComboBox<FamilyTreeDTO> cmbFamilyTrees;
    private javax.swing.JComboBox<NodeDTO> cmbFemaleMembers;
    private javax.swing.JLayeredPane layeredPane;
    private javax.swing.JLabel lblChooseMember;
    private javax.swing.JLabel lblChooseTree;
    private javax.swing.JLabel lblInstruction;
    private javax.swing.JLabel lblMessage;
    private javax.swing.JLabel lblSpouseAdded;
    private javax.swing.JPanel pnlAddSpouse;
    private javax.swing.JPanel pnlSpouseAdded;
    // End of variables declaration//GEN-END:variables

    private void preparePanel() {
        pnlSpouseAdded.setVisible(false);
    }

    public void showExistingSpouse(String spouseInfo){
        pnlAddSpouse.setVisible(false);
        pnlSpouseAdded.setVisible(true);
        lblSpouseAdded.setText("Trenutna supruga: " + spouseInfo);
    }

    public void enableSpouseAdding(){
        pnlAddSpouse.setVisible(true);
        pnlSpouseAdded.setVisible(false);
    }

    private void populateCombos() {
        cmbFamilyTrees.setModel(new DefaultComboBoxModel<>(trees));
        cmbFamilyTrees.setSelectedIndex(-1);
    }
}
