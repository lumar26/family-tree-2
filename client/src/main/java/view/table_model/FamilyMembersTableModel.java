package view.table_model;


import model.impl.Member;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class FamilyMembersTableModel extends AbstractTableModel {

    private List<Member> children;
    private final String[] columnNames = new String[]{"Ime", "Pol", "Datum rođenja"};

    public FamilyMembersTableModel() {
    }

    public void setchildren(List<Member> children) {
        this.children = children;
    }

    @Override
    public int getRowCount() {
        if (children != null)
            return children.size();
        return 0;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int row, int col) {
        Member item = children.get(row);
        switch (col) {
            case 0:
                return item.getName();
            case 1:
                return item.getGender().toString();
            case 2:
                return item.getBirthDate().toString();
            default:
                return "nije naznačeno";
        }
    }

    public List<Member> getRelationships() {
        return children;
    }

    public void addItem(Member newMember) {
        children.add(newMember);
        int insertedRow = children.size() - 1;
        fireTableRowsInserted(insertedRow, insertedRow);
    }

    public void removeItem(int row) {
        children.remove(row);
        fireTableRowsDeleted(row, row);
    }
}
