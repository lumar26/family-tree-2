package main;

import server.Server;
import ui.controller.Controller;

import java.io.IOException;

public class ServerMain {
    public static void main(String[] args) throws IOException {
//        (new Server()).start();
        Controller.getInstance().show();
    }
}
