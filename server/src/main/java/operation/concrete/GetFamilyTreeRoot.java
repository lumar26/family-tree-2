package operation.concrete;

import dto.impl.TreeNodeDTO;
import mapper.NodeDtoMapper;
import model.Entity;
import model.impl.Node;
import network.RequestType;
import operation.Operation;
import validation.impl.structural.concrete.FamilyTreeExistsValidator;

import java.util.HashMap;
import java.util.Map;

public class GetFamilyTreeRoot extends Operation {

    private long treeID;

    @Override
    protected void adjustReceivedData() {
        treeID = (long) data;
    }

    @Override
    protected Object operation() throws Exception {
        Map<Long, Entity> nodes = repository.findUnderCondition("tree_id = " + treeID, Node.class);
        Map<Long, Node> nodesOfTree = new HashMap<>();
        if (nodes == null || nodes.isEmpty()) throw new Exception("Izabrani rodoslov nema članove za prikaz");
        nodes.forEach((id, entity) -> nodesOfTree.put(id, (Node) entity));
        /*prolazimo kroz listu čvorova i ukoliko neki od njih ima oca, taj čvor ćemo dodati u listu dece kod čvora - oca*/
        nodesOfTree.forEach((id, node) -> {
            String[] ancestors = node.getAncestry().split("/");
            if (ancestors.length < 2) return;
            Long parentId = Long.parseLong(ancestors[1]);
            nodesOfTree.get(parentId).getChildren().put(node.getId(), node);
        });

        Node rootNode = nodesOfTree.values().stream()
                .filter(node -> node.getAncestry().equals("/"))
                .findFirst()
                .get();

        return NodeDtoMapper.toTreeNodeDto(rootNode);
    }

    @Override
    protected void checkConstraints() throws Exception {
        (new FamilyTreeExistsValidator(repository, treeID)).validate();
    }
}
