package operation.concrete;

import dto.impl.FamilyTreeDTO;
import dto.impl.FamilyTreeDataDTO;
import mapper.FamilyTreeDtoMapper;
import model.impl.FamilyTree;
import operation.Operation;
import validation.Validator;
import validation.impl.structural.concrete.UserExistsValidator;
import validation.impl.value.concrete.DateInPastValidator;
import validation.impl.value.concrete.NonEmptyStringValidator;
import validation.impl.value.concrete.NotNullObjectValidator;

import java.sql.SQLException;

public class SaveFamilyTree extends Operation {

    private FamilyTreeDataDTO treeData;

    @Override
    protected void adjustReceivedData() {
        treeData = (FamilyTreeDataDTO) data;
    }

    @Override
    protected FamilyTreeDTO operation() throws SQLException{
        FamilyTree saved = (FamilyTree) repository.save(treeData.toEntity());
        return FamilyTreeDtoMapper.toFamilyTreeDto(saved);
    }

    @Override
    protected void checkConstraints() throws Exception {
        Validator baseValidator = new NotNullObjectValidator(treeData.getTitle());
        baseValidator
                .setNext(new UserExistsValidator(repository, treeData.getUserID()))
                .setNext(new NonEmptyStringValidator(treeData.getTitle()))
                .setNext(new DateInPastValidator(treeData.getStartDate()))
                .setNext(new UserExistsValidator(repository, treeData.getUserID()));
        baseValidator.validate();
    }

}
