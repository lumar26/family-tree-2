package operation.concrete;

import dto.impl.MemberInfoDTO;
import dto.impl.NodeSaveDTO;
import dto.impl.NodeDTO;
import mapper.MemberDtoMapper;
import mapper.NodeDtoMapper;
import model.*;
import model.enums.Gender;
import model.enums.Role;
import model.impl.*;
import operation.Operation;
import validation.Validator;
import validation.impl.structural.concrete.ParentChildValidator;
import validation.impl.structural.concrete.RequiredFamilyMemberValidator;
import validation.impl.structural.concrete.SingleParentValidator;
import validation.impl.structural.concrete.UnmarriedMemberValidator;
import validation.impl.value.concrete.AllAlphabetValidator;
import validation.impl.value.concrete.DateInPastValidator;
import validation.impl.value.concrete.LegalDatesSpanValidator;
import validation.impl.value.concrete.NotNullObjectValidator;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class SaveNode extends Operation {

    private NodeSaveDTO newNodeDTO;
    private Member newMember;

    @Override
    protected void adjustReceivedData() {
        newNodeDTO = (NodeSaveDTO) data;
        MemberInfoDTO memberData = newNodeDTO.getMemberData();
        newMember = MemberDtoMapper.toEntity(memberData);
    }

    @Override
    protected void checkConstraints() throws Exception {
        Validator base = new AllAlphabetValidator(newMember.getName());
        base.setNext(new AllAlphabetValidator(newMember.getSurname()))
                .setNext(new NotNullObjectValidator(newMember.getBirthDate()))
                .setNext(new DateInPastValidator(newMember.getBirthDate()))
                .setNext(new DateInPastValidator(newMember.getDeathDate()))
                .setNext(new LegalDatesSpanValidator(newMember.getBirthDate(), newMember.getDeathDate()))
                .setNext(new ParentChildValidator(newMember,
                        newNodeDTO.getFamilyMember() == null ? null : (Node) repository.findById(newNodeDTO.getFamilyMember().getId(), Node.class),
                        newNodeDTO.getFamilyMember() == null ? null : newNodeDTO.getFamilyMember().getRole()))
                /*provera da li su ziveli u odgovarajuce vreme kako bi bili u odnosu roditelj - dete*/
                .setNext(new RequiredFamilyMemberValidator(repository, newNodeDTO.getTreeID(),
                        newNodeDTO.getFamilyMember() == null ? 0 : newNodeDTO.getFamilyMember().getId()))
                /*ako stablo ima clanove mora da postoji familyMemberNode !+ null*/
                .setNext(new UnmarriedMemberValidator(repository, 0, newNodeDTO.getSpouseID()))
                /*ako je dodeljen supruznik on ne bi smeo da ima supruznika vec*/
                .setNext(new SingleParentValidator(repository, newNodeDTO.getFamilyMember().getId(), newNodeDTO.getFamilyMember().getRole()));
        /*ako je clan porodice dete ne sme vec da ima oca, nego ovaj clan mora da mu bude otac*/
        base.validate();


        /* todo: ukoliko clan ima roditelja definisanog u familyMemberNode, taj roditelj mora da ima supruznika tj. zenu
         * todo: dete mora da ima i majku i oca definisane*/
    }


    @Override
    protected NodeDTO operation() throws Exception {
        /*najpre mora da se sacuva clan*/
        Member m = (Member) repository.save(newMember);
        System.err.println("Nakon sto je Member sacuvan: " + m.getBirthPlace());
        Node newNode = new Node(
                (FamilyTree) repository.findById(newNodeDTO.getTreeID(), FamilyTree.class),
                newNodeDTO.getAncestry(),
                newMember,
                (User) repository.findById(newNodeDTO.getUserID(), User.class)
        );
        // čuvanje čvora
        Node savedNode = (Node) repository.save(newNode);

//        Ukoliko je definisana supruga za člana onda treba dodati i brak u bazu
        Node spouse = (Node) repository.findById(newNodeDTO.getSpouseID(), Node.class);
        if (spouse != null) {
            if (newNode.getMember().getGender().equals(Gender.MALE))
                repository.save(new Marriage(newNode.getId(), spouse.getId())); // pazimo na redosled ko je muz a ko je zena kako bi muz uvek drzao primarni kljuc
            else
                repository.save(new Marriage(spouse.getId(), newNode.getId()));
        }
//      Povezivanje sa članom porodice, ukoliko nije prvi član u stablu
        Node familyMemberNode = (Node) repository.findById(newNodeDTO.getFamilyMember().getId(), Node.class);
        Role familyMemberRole = newNodeDTO.getFamilyMember().getRole();
        if (familyMemberNode != null && familyMemberRole != null) {
            handleAncestry(familyMemberNode, familyMemberRole, savedNode);
        }
        return NodeDtoMapper.toNodeDto(savedNode);
    }

    /**
     * @param familyMemberNode Čvor koji predstavlja člana porodice sa kojim se povezuje čvor koji se unosi
     * @param familyMemberRole Uloga gore navedenog člana porodice
     * @param savedNode        Čvor koji je sačuvan i kog treba dodati u rodoslov preko ažuriranja polja "ancestry"
     * @throws SQLException Greška prilikom rada sa bazom podataka
     */
    private void handleAncestry(Node familyMemberNode, Role familyMemberRole, Node savedNode) throws SQLException {
        /*da bismo popunili polje ancestry trebe da ga izracunamo preko clana porodice*/
        if (familyMemberRole.equals(Role.CHILD)) {
            /*
             * ako je novi cvor roditelj i povezali smo ga sa detetom onda ancestry ostaje '/' dok detetu treba promeniti polje ancestry
             */
            updateChildrenAncestry(savedNode);
        }
        if (familyMemberRole.equals(Role.PARENT)) {
            /*
             * ako je novi cvor dete, tj. onaj sa kojim smo ga povezali ima ulogu roditelja
             * ondak treba samo da azuriramo ancestry na node koji smo dodali
             */
            savedNode.setAncestry("/" + familyMemberNode.getId() + familyMemberNode.getAncestry());
            repository.update(savedNode, savedNode.getId());
        }
    }

    /**
     * Ukoliko je čvor koji se unosi roditelj drugim čvorovima, polje ancestry u tim drugim čvorovima se mora ažurirati
     *
     * @param node instanca čvora koji je već sačuvan u bazi
     * @throws SQLException Problem pri radu sa bazom podataka
     */
    private void updateChildrenAncestry(Node node) throws SQLException {

        Map<Long, Entity> entities = repository.findUnderCondition(
                "tree_id = " + node.getFamilyTree().getId() + "" + " and n.id <> " + node.getId(),
                Node.class);
//
        Map<Long, Node> children = new HashMap<>();

        if (entities == null) return;  // todo: rešiti na neki drugi način

        entities.forEach((id, entity) -> children.put(id, (Node) entity));

        children.values().forEach(child -> {
            child.setAncestry(child.getAncestry() + node.getId() + "/");
            try {
                repository.update(child, child.getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


}
