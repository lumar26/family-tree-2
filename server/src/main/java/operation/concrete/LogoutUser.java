package operation.concrete;

import operation.Operation;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;

public class LogoutUser extends Operation {

    private Socket socket;

    @Override
    protected void adjustReceivedData() {
        socket = (Socket) data;
    }

    @Override
    protected Object operation() throws SQLException {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void checkConstraints() throws Exception {

    }
}
