package operation.concrete;

import dto.impl.FamilyTreeDTO;
import dto.impl.UserDTO;
import mapper.FamilyTreeDtoMapper;
import model.Entity;
import model.impl.FamilyTree;
import operation.Operation;
import validation.impl.structural.concrete.UserExistsValidator;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class GetTreesOfUser extends Operation {

    private UserDTO user;

    @Override
    protected void adjustReceivedData() {
        user = (UserDTO) data;
    }

    @Override
    protected FamilyTreeDTO[] operation() throws SQLException {
        Map<Long, Entity> entites = repository.findUnderCondition("user_id = " + user.getUserId(), FamilyTree.class);
        Map<Long, FamilyTree> trees = new HashMap<>();
        entites.forEach((id, en) -> trees.put(id, (FamilyTree) en));

        FamilyTree[] treesArray = trees.values().toArray(FamilyTree[]::new);
        return FamilyTreeDtoMapper.toFamilyTreeDtoList(treesArray);
    }

    @Override
    protected void checkConstraints() throws Exception {
        /*da li postoji prosledjeni user?*/
        (new UserExistsValidator(repository, user.getUserId())).validate();
    }
}
