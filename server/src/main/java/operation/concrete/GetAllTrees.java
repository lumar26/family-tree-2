package operation.concrete;

import dto.impl.FamilyTreeDTO;
import model.Entity;
import model.impl.FamilyTree;
import network.RequestType;
import operation.Operation;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class GetAllTrees extends Operation {

    @Override
    protected void adjustReceivedData() {
    }

    @Override
    protected Object operation() throws SQLException {
        Map<Long, Entity> entites = repository.findAll(FamilyTree.class);
        Map<Long, FamilyTree> trees = new HashMap<>();
        entites.forEach((id, en) -> trees.put(id, (FamilyTree) en));

        FamilyTree[] treeArray =trees.values().toArray(FamilyTree[]::new);
        FamilyTreeDTO[] dtos = new FamilyTreeDTO[treeArray.length];
        for (int i = 0; i < treeArray.length; i++) {
            dtos[i] = new FamilyTreeDTO(treeArray[i].getId(), treeArray[i].getName());
        }
        return dtos;
    }

    @Override
    protected void checkConstraints() throws Exception {
    }
}
