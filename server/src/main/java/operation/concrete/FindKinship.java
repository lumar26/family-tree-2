package operation.concrete;

import dto.impl.NodeDTO;
import dto.impl.kinship.concrete.LateralKinshipDTO;
import dto.impl.kinship.concrete.VerticalKinshipDTO;
import model.impl.Node;
import model.enums.KinshipType;
import network.RequestType;
import operation.Operation;

import java.util.ArrayList;
import java.util.List;

public class FindKinship extends Operation {

    private NodeDTO[] nodePair;

    @Override
    protected void adjustReceivedData() {
        nodePair = (NodeDTO[]) data;
    }

    @Override
    protected Object operation() throws Exception {
        NodeDTO nodeDTO1 = nodePair[0];
        NodeDTO nodeDTO2 = nodePair[1];

        Node node1 = (Node) repository.findById(nodeDTO1.getNodeID(), Node.class);
        Node node2 = (Node) repository.findById(nodeDTO2.getNodeID(), Node.class);


        List<Long> ancestryList1 = new ArrayList<>();
        List<Long> ancestryList2 = new ArrayList<>();

        String[] ancestry1 = node1.getAncestry().split("/");
        String[] ancestry2 = node2.getAncestry().split("/");

        for (String strId : ancestry1) {
            if (!strId.equals(""))
            ancestryList1.add(Long.parseLong(strId));
        }
        for (String strId : ancestry2) {
            if (!strId.equals("") )
                ancestryList2.add(Long.parseLong(strId));
        }

        if (ancestryList1.contains(node2.getId())) {
            System.out.println(nodeDTO2 + " je predak od " + nodeDTO1);
            return  new VerticalKinshipDTO(KinshipType.VERTICAL, Math.abs(ancestryList2.size() - ancestryList1.size()), nodeDTO2, nodeDTO1);
        }

        if (ancestryList2.contains(node1.getId())) {
            System.out.println(nodeDTO1 + " je predak od " + nodeDTO2);
            return new VerticalKinshipDTO(KinshipType.VERTICAL, Math.abs(ancestryList2.size() - ancestryList1.size()), nodeDTO1, nodeDTO2);
        }
            long common = -1;
            for (Long l : ancestryList1) {
                if (ancestryList2.contains(l)) {
                    common = l;
                    break;
                }
            }
            if (common == -1)
                throw new Exception("Nije moguće pronaći rodbinsku vezu za: " + nodeDTO1 + " i " + nodeDTO2);
            return new LateralKinshipDTO(KinshipType.LATERAL, Math.max(ancestryList1.indexOf(common), ancestryList2.indexOf(common)) + 1, ancestryList1.size() == ancestryList2.size());
    }

    @Override
    protected void checkConstraints() throws Exception {
        if (nodePair.length != 2)
            throw new Exception("Klijent je prosledio " + nodePair.length + " umesto 2 člana za proveru njihovog srodstva");

    }
}
