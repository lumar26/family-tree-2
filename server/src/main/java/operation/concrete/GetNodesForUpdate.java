package operation.concrete;

import dto.impl.FamilyTreeDTO;
import dto.impl.NodeDTO;
import dto.impl.NodeUpdateDTO;
import mapper.NodeDtoMapper;
import model.Entity;
import model.impl.Marriage;
import model.impl.Node;
import model.enums.Gender;
import operation.Operation;
import validation.impl.structural.concrete.FamilyTreeExistsValidator;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class GetNodesForUpdate extends Operation {
    private FamilyTreeDTO tree;

    @Override
    protected void adjustReceivedData() {
        tree = (FamilyTreeDTO) data;
    }

    @Override
    protected NodeUpdateDTO[] operation() throws Exception {
        Map<Long, Entity> entityMap = repository.findUnderCondition("tree_id = " + tree.getTreeID(), Node.class);
        Map<Long, Node> nodes = new HashMap<>();
        entityMap.forEach((id, en) -> nodes.put(id, (Node) en));
        Node[] nodesOfUser = nodes.values().toArray(Node[]::new);
        NodeUpdateDTO[] nodesWithoutSpouses = NodeDtoMapper.toNodeUpdateDtoList(nodesOfUser);
        addSpouses(nodesWithoutSpouses);
        return nodesWithoutSpouses;
    }

    private void addSpouses(NodeUpdateDTO[] nodesWithoutSpouses) throws SQLException {
        for (NodeUpdateDTO n : nodesWithoutSpouses) {
            /*svaki cvor koji ima supruznika mora da se zabelezi i da se doda u nodeupdatedto objekat*/
            Map<Long, Entity> spouses = null;
            if (n.getMemberData().getGender().equals(Gender.MALE))
                spouses = repository.findUnderCondition(
                        String.format("husband_id = %d", n.getNodeId()), Marriage.class);
            if (n.getMemberData().getGender().equals(Gender.FEMALE))
                spouses = repository.findUnderCondition(
                        String.format("wife_id = %d", n.getNodeId()), Marriage.class);


            if (spouses != null && !spouses.isEmpty()) {
                Marriage marriage = (Marriage) spouses.values().stream().findFirst().get();
                Node spouse = (Node) repository.findById(marriage.getWifeID(), Node.class);
                n.setSpouseID(spouse.getId());
                n.setSpouseInfo(spouse.toString());
            }

        }
    }

    @Override
    protected void checkConstraints() throws Exception {
        /*da li postoji stablo?*/
        (new FamilyTreeExistsValidator(repository, tree.getTreeID())).validate();
    }
}
