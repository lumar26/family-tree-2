package operation.concrete;

import dto.impl.FamilyTreeDTO;
import mapper.FamilyTreeDtoMapper;
import model.impl.FamilyTree;
import operation.Operation;
import validation.impl.structural.concrete.EmptyFamilyTreeValidator;

public class DeleteFamilyTree extends Operation {

    private FamilyTreeDTO treeDTO;

    @Override
    protected void adjustReceivedData() {
        treeDTO = (FamilyTreeDTO) data;
    }

    @Override
    protected FamilyTreeDTO operation() throws Exception {
        FamilyTree deleted = (FamilyTree) repository.delete(treeDTO.getTreeID(), FamilyTree.class);
        if (deleted == null) throw new Exception("Nije moguće izbrisati rodoslov: " + treeDTO.getDisplayText());
        return FamilyTreeDtoMapper.toFamilyTreeDto(deleted);
    }

    @Override
    protected void checkConstraints() throws Exception {
        /*provera da li stablo sadrzi clanove, i ako sadrzi vraca se greska */
        (new EmptyFamilyTreeValidator(repository, treeDTO.getTreeID())).validate();
    }
}
