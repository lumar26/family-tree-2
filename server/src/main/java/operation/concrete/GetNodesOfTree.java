package operation.concrete;

import mapper.NodeDtoMapper;
import model.Entity;
import model.impl.Node;
import operation.Operation;
import validation.impl.structural.concrete.FamilyTreeExistsValidator;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class GetNodesOfTree extends Operation {

    private long treeID;

    @Override
    protected void adjustReceivedData() {
        treeID = (long) data;
    }

    @Override
    protected Object operation() throws SQLException {
        Map<Long, Entity> entityMap = repository.findUnderCondition("tree_id = " + treeID, Node.class);
        Map<Long, Node> nodes = new HashMap<>();
        entityMap.forEach((id, en) -> nodes.put(id, (Node) en));

        /*prolazimo kroz listu čvorova i ukoliko neki od njih ima oca, taj čvor ćemo dodati u listu dece kod čvora - oca*/
        nodes.forEach((id, node) -> {
            String[] ancestors = node.getAncestry().split("/");
            if (ancestors.length < 2) return;
            Long parentId = Long.parseLong(ancestors[1]);
            nodes.get(parentId).getChildren().put(id, node);
        });


        Node[] result = nodes.values().toArray(Node[]::new);
        return NodeDtoMapper.toNodeDtoList(result);
    }

    @Override
    protected void checkConstraints() throws Exception {
        /*da li postoji prosledjeno stablo?*/
        (new FamilyTreeExistsValidator(repository, treeID)).validate();
    }
}
