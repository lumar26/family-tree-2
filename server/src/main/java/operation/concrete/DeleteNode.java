package operation.concrete;

import dto.impl.NodeDTO;
import mapper.NodeDtoMapper;
import model.impl.Member;
import model.impl.Node;
import network.RequestType;
import operation.Operation;
import validation.impl.structural.concrete.SafeNodeDeletionValidator;

import java.sql.SQLException;

public class DeleteNode extends Operation {
    private NodeDTO nodeDTO;
    private Node toDelete;

    @Override
    protected void adjustReceivedData() {
        nodeDTO = (NodeDTO) data;
    }

    @Override
    protected NodeDTO operation() throws SQLException {
        /*uklanjanje cvora iz liste dece njegovog roditelja*/


        Node deleted = (Node) repository.delete(toDelete.getId(), Node.class); // baca gresku ukoliko postoji od supruznika fk ka tom cvoru
        Member deletedMember = (Member) repository.delete(toDelete.getMember().getId(), Member.class);
        // mora ipak prvo da se obrise node jer node sadrzi referencu na member pa bi verovatno bacilo gresku
        return NodeDtoMapper.toNodeDto(deleted);
    }

    @Override
    protected void checkConstraints() throws Exception {
        toDelete = (Node) repository.findById(nodeDTO.getNodeID(), Node.class);
        (new SafeNodeDeletionValidator(repository, toDelete)).validate();
    }
}
