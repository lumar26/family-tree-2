package operation.concrete;

import dto.impl.*;
import mapper.FamilyTreeDtoMapper;
import mapper.MemberDtoMapper;
import mapper.NodeDtoMapper;
import model.Entity;
import model.impl.Marriage;
import model.impl.Node;
import model.enums.Gender;
import operation.Operation;

import java.sql.SQLException;
import java.util.Map;

public class GetNodeInfo extends Operation {

    private long nodeId;

    @Override
    protected void adjustReceivedData() {
        nodeId = ((TreeNodeDTO) data).getNodeId();
    }

    @Override
    protected Object operation() throws Exception {
        Node node = (Node) repository.findById(nodeId, Node.class);
        if (node == null)
            throw new Exception("Nije moguće pronaći podatke o članu.");
        Node spouse = findSpouse(node);
        if (spouse == null)
            return new NodeInfoDTO(MemberDtoMapper.toMemberInfoDto(node.getMember()), null, null);
        return new NodeInfoDTO(
                MemberDtoMapper.toMemberInfoDto(node.getMember()),
                NodeDtoMapper.toNodeDto(spouse),
                FamilyTreeDtoMapper.toFamilyTreeDto(spouse.getFamilyTree())
        );
    }

    private Node findSpouse(Node node) {
        Map<Long, Entity> marriageMap = null;
        Marriage marriage = null;
        try {
            if (node.getMember().getGender().equals(Gender.MALE))
                marriageMap = repository.findUnderCondition("husband_id = " + node.getId(), Marriage.class);
            if (node.getMember().getGender().equals(Gender.FEMALE))
                marriageMap = repository.findUnderCondition("wife_id = " + node.getId(), Marriage.class);

            if (marriageMap != null && !marriageMap.isEmpty()) {
                marriage = (Marriage) marriageMap.values().stream().findFirst().get();
                if (marriage.getHusbandID() != nodeId)
                    return (Node) repository.findById(marriage.getHusbandID(), Node.class);
                if (marriage.getWifeID() != nodeId)
                    return (Node) repository.findById(marriage.getWifeID(), Node.class);
            }
        } catch (SQLException ignored) {
        }
        return null;
    }

    @Override
    protected void checkConstraints() throws Exception {

    }
}
