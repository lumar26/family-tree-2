package operation.concrete;

import dto.impl.UserDTO;
import dto.impl.UserLoginDTO;
import exception.UserNotFoundException;
import mapper.UserDtoMapper;
import model.Entity;
import model.impl.User;
import operation.Operation;
import ui.controller.Controller;

import java.sql.SQLException;
import java.util.Map;

public class LoginUser extends Operation {

    private String username;
    private String password;

    @Override
    protected void adjustReceivedData() {
        UserLoginDTO user = (UserLoginDTO) data;
        username = user.getUsername();
        password = user.getPassword();
    }

    @Override
    protected UserDTO operation() throws Exception {
        try {
            Map<Long, Entity> entities = repository.
                    findUnderCondition(String.format("username = '%s' and password = '%s'", username, password), User.class);
            if (entities == null || entities.values().stream().findFirst().isEmpty())
                throw new UserNotFoundException("Ne postoji korisnik sa prosleđenim korisničkim imenom i lozinkom");

            User user = (User) entities.values().stream().findFirst().get();
            Controller.getInstance().addUser(UserDtoMapper.fromEntity(user)); // dodavanje u listu aktivnih korisnika
            return UserDtoMapper.fromEntity(user);
        } catch (SQLException e) {
            throw new Exception("Nije moguće uspostaviti konekciju sa bazom podataka");
        }
    }

    @Override
    protected void checkConstraints() throws Exception {
//        nema ograničenja za proveru
    }
}
