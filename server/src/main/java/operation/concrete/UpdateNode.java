package operation.concrete;

import dto.impl.NodeDTO;
import dto.impl.NodeUpdateDTO;
import mapper.MemberDtoMapper;
import model.Entity;
import model.impl.Marriage;
import model.impl.Member;
import operation.Operation;
import validation.Validator;
import validation.impl.structural.concrete.FamilyTreeExistsValidator;
import validation.impl.structural.concrete.UnmarriedMemberValidator;
import validation.impl.structural.concrete.UserExistsValidator;
import validation.impl.value.concrete.AllAlphabetValidator;
import validation.impl.value.concrete.DateInPastValidator;
import validation.impl.value.concrete.NotNullObjectValidator;

import java.sql.SQLException;
import java.util.Map;

public class UpdateNode extends Operation {

    private Member memberToUpdate;
    private NodeUpdateDTO updatedDTO;

    @Override
    protected void adjustReceivedData() {
        updatedDTO = (NodeUpdateDTO) data;
        memberToUpdate = MemberDtoMapper.toEntity(updatedDTO.getMemberData());
    }

    @Override
    protected NodeDTO operation() throws SQLException {
        /*azuriranje podataka o clanu*/
        Member updatedMember = (Member) repository.update(memberToUpdate, memberToUpdate.getId());

        /*azuriranje braka*/
        if (updatedDTO.getSpouseID() != 0) {
            // u slucaju da je prosledjen i supruznik treba ih zajedno dodati u tabelu marriage, a prethodno izbrisati postojeci brak ako je postojao
            Map<Long, Entity> marriages =
                    repository.findUnderCondition(String.format(
                                    "husband_id = %d or wife_id = %d", updatedDTO.getNodeId(), updatedDTO.getNodeId()),
                            Marriage.class);
            if (marriages != null && !marriages.isEmpty()) {
                Marriage thisNodesMarriage = (Marriage) marriages.values().stream().findFirst().get();
                repository.deleteUnderCondition("husband_id = " + thisNodesMarriage.getHusbandID(), Marriage.class);
            }
            repository.save(new Marriage(updatedDTO.getNodeId(), updatedDTO.getSpouseID()));
        }

        return new NodeDTO(updatedDTO.getNodeId(), updatedMember.getGender(), updatedMember.toString());
    }

    @Override
    protected void checkConstraints() throws Exception {
        Validator base = new AllAlphabetValidator(memberToUpdate.getName());
        base.setNext(new AllAlphabetValidator(memberToUpdate.getSurname()))
                .setNext(new NotNullObjectValidator(memberToUpdate.getBirthDate()))
                .setNext(new DateInPastValidator(memberToUpdate.getBirthDate()))
                .setNext(new FamilyTreeExistsValidator(repository, updatedDTO.getTreeID()))
                .setNext(new UserExistsValidator(repository, updatedDTO.getUserID()))
                .setNext(new UnmarriedMemberValidator(repository, updatedDTO.getNodeId(), updatedDTO.getSpouseID()));
        base.validate();
    }
}
