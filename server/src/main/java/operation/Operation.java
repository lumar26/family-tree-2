package operation;

import network.Response;
import network.ResponseStatus;
import repository.Repository;

import java.sql.SQLException;

public abstract class Operation {

    protected Object data;
    protected Repository repository = new Repository();

    public void setData(Object data) {
        this.data = data;
    }

    /**
     * Template metoda koja predstavlja izrvšenje jednog korisničkog zahteva
     *
     * @return odgovor za klijenta : Response
     */
    public Response execute() {
        try {
            adjustReceivedData(); // prebacivanje podataka iz zahteva u odgovarajući format
            checkConstraints(); // provera vrednosnih i strukturnih ograničenja
            Object responseData = operation();
            commitTransaction(); // potvrda transakcije
            return new Response(responseData, ResponseStatus.SUCCESS); // odgovor na zahtev koji se vraća klijentu
        } catch (Exception e) {
            try {
                rollbackTransaction(); // poništavanje transakcije ukoliko je bila neuspešna
            } catch (SQLException ex) {
                return new Response(null, ResponseStatus.ERROR, ex);
            }
            return new Response(null, ResponseStatus.ERROR, e);
        }
    }

    protected abstract void adjustReceivedData();

    private void rollbackTransaction() throws SQLException {
        if (repository != null)
            repository.rollback();
    }

    private void commitTransaction() throws SQLException {
        repository.commit();
    }

    /**
     * @return Objekat koji predstavlja rezultat sistemske operacije: DTO ili kolekciju DTO
     * @throws Exception Greška prilikom izvršenja operacije, sadrži poruku o grešci namenjenu klijentu
     */
    protected abstract Object operation() throws Exception;

    protected abstract void checkConstraints() throws Exception;

}
