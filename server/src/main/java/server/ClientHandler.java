package server;

import dto.impl.UserDTO;
import network.Request;
import network.RequestType;
import network.Response;
import network.ResponseStatus;
import operation.Operation;
import operation.concrete.*;
import ui.controller.Controller;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

public class ClientHandler extends Thread {
    private final Socket socket;

    private final ObjectOutputStream toClient;
    private final ObjectInputStream fromClient;
    private final Server server;
    private boolean running;

    private UserDTO loggedUser;

    public ClientHandler(Socket socket, Server server) throws IOException {
        this.socket = socket;
        toClient = new ObjectOutputStream(socket.getOutputStream());
        fromClient = new ObjectInputStream(socket.getInputStream());
        this.server = server;
    }

    @Override
    public void run() {
        running = true;
        try {
            while (running) {
                Request req = (Request) fromClient.readObject();
                Response response = handleRequest(req);

                if (response == null) break; // U slučaju odjavljivanja

                if (response.getData() instanceof UserDTO) loggedUser = (UserDTO) response.getData(); // U slučaju prijavljivanja, tada je odgovor u obliku UserDTO

                toClient.writeObject(response);
            }
        } catch (EOFException | SocketException ex) {
            System.out.println("Korisnik se odjavio.");
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Obrađuje korisnički zahtev u zavisnosti od tipa zahteva
     *
     * @param request Zahtev koji stiže od klijenta : Request
     * @return Odgovor za klijenta : Response
     */
    private Response handleRequest(Request request) {
        Operation operation;
        switch (request.getOperation()) {
            case LOGIN:
                operation = new LoginUser();
                break;
            case SAVE_FAMILY_TREE:
                operation = new SaveFamilyTree();
                break;
            case GET_ALL_TREES:
                operation = new GetAllTrees();
                break;
            case GET_NODES_OF_TREE:
                operation = new GetNodesOfTree();
                break;
            case SAVE_NODE:
                operation = new SaveNode();
                break;
            case UPDATE_NODE:
                operation = new UpdateNode();
                break;
            case GET_NODES_FOR_UPDATE:
                operation = new GetNodesForUpdate();
                break;
            case GET_TREES_OF_USER:
                operation = new GetTreesOfUser();
                break;
            case DELETE_FAMILY_TREE:
                operation = new DeleteFamilyTree();
                break;
            case DELETE_NODE:
                operation = new DeleteNode();
                break;
            case GET_FAMILY_TREE_ROOT:
                operation = new GetFamilyTreeRoot();
                break;
            case FIND_KINSHIP:
                operation = new FindKinship();
                break;
            case GET_NODE_INFO:
                operation = new GetNodeInfo();
                break;
            case LOGOUT:
                logout();
                return null;
            default:
                return new Response(null, ResponseStatus.ERROR, new Exception("Korisnički zahtev nije prepoznat."));
        }
        operation.setData(request.getData());
        return operation.execute();
    }

    private void logout() {
        server.getAllUsers().remove(loggedUser);
        Controller.getInstance().removeUser(loggedUser);
        try {
            toClient.close();
            fromClient.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
            // TODO: prikaz problema
        }
        running = false;
    }

    public Socket getSocket() {
        return socket;
    }

    public UserDTO getLoginUser() {
        return loggedUser;
    }
}
