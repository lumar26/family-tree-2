package server;

import dto.impl.UserDTO;
import prop.TransferProperties;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server extends Thread {
    private final ServerSocket serverSocket;
    private final List<ClientHandler> clients;
    private boolean running = true;

    public Server() throws IOException {
        serverSocket = new ServerSocket(Integer.parseInt(TransferProperties.getInstance().getProperty("server_port")));
        clients = new ArrayList<>();
    }

    @Override
    public void run() {
        System.out.println("Server je osluškuje zahteve na portu " + serverSocket.getLocalPort());
        while (running) {
            try {
                Socket socket = serverSocket.accept();
                ClientHandler handler = new ClientHandler(socket, this);
                clients.add(handler);
                handler.start();
            }  catch (IOException ex) {
                System.err.println(ex.getMessage());
            }
        }
        stopAllThreads();
    }


    private void stopAllThreads() {
        for (ClientHandler client : clients) {
            try {
                client.getSocket().close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void terminate() {
        running = false;
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<UserDTO> getAllUsers() {
        List<UserDTO> users = new ArrayList<>();

        for (ClientHandler client : clients) {
            users.add(client.getLoginUser());
        }

        return users;
    }
}
