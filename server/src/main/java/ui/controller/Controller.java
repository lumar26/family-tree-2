package ui.controller;

import dto.impl.UserDTO;
import prop.TransferProperties;
import server.Server;
import ui.table.model.UsersTableModel;
import ui.view.ServerForm;

import java.io.IOException;

public class Controller {
    private static Controller instance;

    private final ServerForm form;
    private Server server;

    private Controller() {
        form = new ServerForm();
        setListeners();
    }

    public static Controller getInstance() {
        if (instance == null) instance = new Controller();
        return instance;
    }
    
    public void show(){
        form.setVisible(true);
    }

    private void setListeners() {
        form.getBtnStartServer().addActionListener(a -> startServer());
        form.getBtnStopServer().addActionListener(a -> stopServer());
    }

    private void stopServer() {
        if (((UsersTableModel)form.getTblActiveUsers().getModel()).getActiveUsers().size() > 0){
            form.printError("Nije bezbedno zaustaviti server jer postoje aktivni korisnici");
            return;
        }
        server.terminate();
        form.serverStopped();
    }

    private void startServer() {
        try {
            TransferProperties.getInstance().setProperty("server_port", String.valueOf(form.getTxtPort().getValue()));
            server = new Server();
        } catch (IOException e) {
            form.printError(e.getMessage());
            return;
        }

        server.start();
        form.serverStarted();
    }

    public void addUser(UserDTO user){
        ((UsersTableModel)form.getTblActiveUsers().getModel()).addUser(user);
    }

    public void removeUser(UserDTO user){
        ((UsersTableModel)form.getTblActiveUsers().getModel()).removeUser(user);
    }

}
