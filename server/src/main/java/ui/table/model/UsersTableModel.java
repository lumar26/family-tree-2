package ui.table.model;

import dto.impl.UserDTO;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class UsersTableModel extends AbstractTableModel {

    private final String[] columnNames = new String[]{"Korisničko ime", "Email adresa", "ID"};

    private List<UserDTO> activeUsers = new ArrayList<>();

    public void setActiveUsers(List<UserDTO> activeUsers) {
        this.activeUsers = activeUsers;
    }

    public List<UserDTO> getActiveUsers() {
        return activeUsers;
    }

    public void addUser(UserDTO user) {
        activeUsers.add(user);
        fireTableDataChanged();
    }

    public void removeUser(UserDTO user) {
        activeUsers.remove(user);
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public int getRowCount() {
        return activeUsers.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        UserDTO u = activeUsers.get(row);
        switch (col) {
            case 0:
                return u.getUsername();
            case 1:
                return u.getEmail();
            case 2:
                return u.getUserId();
            default:
                return "N/A";
        }
    }
}