package model.impl;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import model.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

@JsonSerialize
public class User implements Entity {
    private static final String insertAttributes = "username, password, email, name, surname";
    private static final String table = "user";

    private long id;
    private String username;
    private String password;
    private String email;
    private String name;
    private String surname;

    public User() {
    }

    public User(long id, String username, String password, String email, String name, String surname) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.surname = surname;
    }

    public User(String username, String password, String email, String name, String surname) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.surname = surname;
    }

    public static String getDBTableName() {
        return "user";
    }

    public static String getTableAttributesForInsert() {
        return "username, password, email, name, surname";
    }

    public static String getAllTableAttributes() {
        return "id, " + getTableAttributesForInsert();
    }

    public static String getQuery(String condition) {
        return "select u.id, u.username, u.password, u.email, u.name, u.surname from user u where " + condition + ";";
    }

    public static User toEntity(ResultSet rs) throws SQLException {
        /*podaci o korisniku*/
        long id = rs.getLong("u.id");
        String username = rs.getString("u.username");
        String password = rs.getString("u.password");
        String email = rs.getString("u.email");
        String name = rs.getString("u.name");
        String surname = rs.getString("u.surname");

        return new User(id, username, password, email, name, surname);
    }

    @Override
    public String queryInsert() {
        return String.format("insert into %s (%s) values ('%s', '%s', '%s', '%s', '%s', '%s');", table, insertAttributes,
                name, surname, password, email, name, surname);
    }

    @Override
    public String queryUpdate(long primaryKey) {
        return String.format("update %s set " +
                        "username = '%s', " +
                        "password = '%s', " +
                        "email = '%s, " +
                        "name = '%s, " +
                        "surname = '%s " +
                        "where id = '%s';",
                table, username, password, email, name, surname, primaryKey);
    }

    public static String deleteQuery(Long id){
        return String.format("delete from %s where id = %d;", table, id);
    }

    public static String queryFind(String condition) {
        return  "select u.id, u.username, u.password, u.email, u.name, u.surname" +
                " from user u " +
                " where u." + condition + ";";
    }

    public static String queryFindAll() {
        return  "select u.id, u.username, u.password, u.email, u.name, u.surname from user u;";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId() == user.getId() && getUsername().equals(user.getUsername()) && getPassword().equals(user.getPassword()) && Objects.equals(getEmail(), user.getEmail());
    }
}
