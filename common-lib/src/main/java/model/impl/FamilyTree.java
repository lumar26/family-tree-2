package model.impl;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import model.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

@JsonSerialize
public class FamilyTree implements Entity {

    private static final String insertAttributes = "name, description, date_from, user_id";
    private static final String table = "tree";

    private long id;
    private String name;
    private String description;
    private LocalDate dateFrom;
    private User user;

    public FamilyTree() {
    }

    public FamilyTree(long id, String name, String description, LocalDate dateFrom, User user) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateFrom = dateFrom;
        this.user = user;
    }

    public FamilyTree(String name, String description, LocalDate dateFrom, User user) {
        this.name = name;
        this.description = description;
        this.dateFrom = dateFrom;
        this.user = user;
    }

    public static String getDBTableName() {
        return "tree";
    }

    public static String getTableAttributesForInsert() {
        return "name, description, date_from, user_id";
    }

    public static String getAllTableAttributes() {
        return "id, " + getTableAttributesForInsert();
    }

    public static String getQuery(String condition) {
        return "select t.id, t.name, t.description, t.date_from, t.user_id, u.username, u.password, u.email, u.name, u.surname" +
                " from tree t join user u on t.user_id = u.id" +
                " where " + condition + ";";
    }

    public static FamilyTree toEntity(ResultSet rs) throws SQLException {
        /*podaci o kreatoru stabla*/
        long userID = rs.getLong("t.user_id");
        String username = rs.getString("u.username");
        String password = rs.getString("u.password");
        String email = rs.getString("u.email");
        String name = rs.getString("u.name");
        String surname = rs.getString("u.surname");
        /*podaci o stablu*/
        long id = rs.getLong("t.id");
        String treeName = rs.getString("t.name");
        String treeDescription = rs.getString("t.description");
        LocalDate treeDateFrom = rs.getDate("t.date_from").toLocalDate();

        User user = new User(userID, username, password, email, name, surname);
        return new FamilyTree(id, treeName, treeDescription, treeDateFrom, user);
    }

    @Override
    public String toString() {
        return "Rodoslov - " + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FamilyTree)) return false;
        FamilyTree that = (FamilyTree) o;
        return getId() == that.getId() && getName().equals(that.getName()) && getUser().equals(that.getUser());
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String queryInsert() {
        return String.format("insert into %s (%s) values ('%s', '%s', '%s', %d);", table, insertAttributes,
                name, description, dateFrom.toString(), user.getId());
    }

    @Override
    public String queryUpdate(long primaryKey) {
        return String.format("update %s set " +
                        "name = '%s'," +
                        "description = '%s', " +
                        "date_from = '%s', " +
                        "user_id = %d " +
                        "where id = %d;",
                table, name, description, dateFrom.toString(), user.getId(), primaryKey);
    }


    public static String deleteQuery(Long id){
        return String.format("delete from %s where id = %d;", table, id);
    }

    public static String queryFind(String condition) {
        return  "select t.id, t.name, t.description, t.date_from, t.user_id," +
                " u.username, u.password, u.email, u.name, u.surname" +
                " from tree t join user u on (t.user_id = u.id) " +
                " where t." + condition + ";";
    }

    public static String queryFindAll() {
        return  "select t.id, t.name, t.description, t.date_from, t.user_id," +
                " u.username, u.password, u.email, u.name, u.surname" +
                " from tree t join user u on (t.user_id = u.id); ";
    }



    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
