package model.impl;

import model.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Marriage implements Entity {
    private static final String table = "marriage";
    private static final String insertAttributes = "husband_id, wife_id";

    private long husbandID;
    private long wifeID;

    public Marriage(long husbandID, long wifeID) {
        this.husbandID = husbandID;
        this.wifeID = wifeID;
    }

    @Override
    public String toString() {
        return "Brak: muž - " + husbandID + " i žena - " + wifeID;
    }

    public static String getDBTableName() {
        return "marriage";
    }

    public static String getTableAttributesForInsert() {
        return "husband_id, wife_id";
    }

    public static String getAllTableAttributes() {
        return getTableAttributesForInsert();
    }

    public static String deleteQuery(Long id) {
        return String.format("delete from %s where husband_id = %d;", table, id);
    }

    public static String conditionalDeleteQuery(String condition){
        return String.format("delete from %s where %s;", table, condition);
    }


    public static String queryFind(String condition) {
        return String.format("select %s from %s where %s;", insertAttributes, table, condition);
    }

    public static String queryFindAll() {
        return "select " + insertAttributes + " from " + table + ";";
    }

    public static Marriage toEntity(ResultSet rs) throws SQLException {
        return new Marriage(rs.getLong("husband_id"), rs.getLong("wife_id"));
    }

    public long getHusbandID() {
        return husbandID;
    }

    public long getWifeID() {
        return wifeID;
    }

    public void setId(long id) {
    }

    @Override
    public long getId() {
        return husbandID;
    }

    @Override
    public String queryInsert() {
        return String.format("insert into %s (%s) values (%d, %d);", table, insertAttributes,
                husbandID, wifeID);
    }

    @Override
    public String queryUpdate(long primaryKey) {
        return String.format("update %s set " +
                        "husband_id = %d, " +
                        "wife_id = %d " +
                        "where id = %d;",
                table, husbandID, wifeID, primaryKey);
    }

}
