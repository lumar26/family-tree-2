package model.impl;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import model.Entity;
import model.enums.Gender;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Objects;

@JsonSerialize
public class Member implements Entity {

    private static final String table = "member";
    private static final String insertAttributes =
            "name, surname, birth_date, death_date, gender, birth_place, death_place, address, biography_url, picture_url";

    private long id;
    private String name;
    private String surname;
    private LocalDate birthDate;
    private LocalDate deathDate;
    private Gender gender;
    private String birthPlace;
    private String deathPlace;
    private String address;
    private String biographyUrl;
    private String pictureUrl;

    public Member() {
    }

    public Member(long id, String name, String surname,
                  LocalDate birthDate, LocalDate deathDate,
                  Gender gender, String birthPlace, String deathPlace,
                  String address, String biographyUrl, String pictureUrl) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
        this.gender = gender;
        this.birthPlace = birthPlace;
        this.deathPlace = deathPlace;
        this.address = address;
        this.biographyUrl = biographyUrl;
        this.pictureUrl = pictureUrl;
    }

    public Member(String name, String surname, LocalDate birthDate, LocalDate deathDate,
                  Gender gender, String birthPlace, String deathPlace, String address,
                  String biographyUrl, String pictureUrl) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
        this.gender = gender;
        this.birthPlace = birthPlace;
        this.deathPlace = deathPlace;
        this.address = address;
        this.biographyUrl = biographyUrl;
        this.pictureUrl = pictureUrl;
    }


    public static String getDBTableName() {
        return "member";
    }

    public static String getTableAttributesForInsert() {
        return "name, surname, birth_date, death_date, gender, birth_place, death_place, address, biography_url, picture_url";
    }

    public static String getAllTableAttributes() {
        return "m.id, " + getTableAttributesForInsert();
    }


    public static Member toEntity(ResultSet rs) throws SQLException {
        /*podaci o clanu*/
        long id = rs.getLong("m.id");
        String name = rs.getString("m.name");
        String surname = rs.getString("m.surname");
        LocalDate birthDate = rs.getDate("m.birth_date").toLocalDate();
        java.sql.Date death = rs.getDate("m.death_date");
        LocalDate deathDate = death == null ? null : death.toLocalDate();
        Gender gender = Gender.valueOf(rs.getString("m.gender"));
        String birthPlace = rs.getString("m.birth_place");
        String deathPlace = rs.getString("m.death_place");
        String address = rs.getString("m.address");
        String biographyUrl = rs.getString("m.biography_url");
        String pictureUrl = rs.getString("m.picture_url");

        return new Member(id, name, surname, birthDate, deathDate, gender,
                birthPlace, deathPlace, address, biographyUrl, pictureUrl);
    }

    @Override
    public String toString() {
        return name + " " + surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Member)) return false;
        Member member = (Member) o;
        return getId() == member.getId() && Objects.equals(getName(), member.getName()) && Objects.equals(getSurname(), member.getSurname()) && getGender() == member.getGender();
    }


    public long getId() {
        return id;
    }

    @Override
    public String queryInsert() {
        if (deathDate == null)
            return String.format("insert into %s " +
                            "(name, surname, birth_date, gender, birth_place, death_place, address, biography_url, picture_url)" +
                            " values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');",
                    table, name, surname, birthDate.toString(), gender, birthPlace, deathPlace, address, biographyUrl, pictureUrl);
        return String.format("insert into %s (%s) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');",
                table, insertAttributes,
                name, surname, birthDate.toString(), deathDate, gender, birthPlace, deathPlace, address, biographyUrl, pictureUrl);
    }

    @Override
    public String queryUpdate(long primaryKey) {
        if (deathDate == null)
            return String.format("update %s set " +
                            "name = '%s'," +
                            "surname = '%s', " +
                            "birth_date = '%s', " +
                            "gender = '%s', " +
                            "birth_place = '%s', " +
                            "death_date = %s, " +
                            "death_place = '%s', " +
                            "address = '%s', " +
                            "biography_url = '%s', " +
                            "picture_url = '%s' " +
                            "where id = %d;",
                    table,
                    name, surname, birthDate.toString(), gender, birthPlace, null, deathPlace, address, biographyUrl, pictureUrl,
                    primaryKey);
        return String.format("update %s set " +
                        "name = '%s'," +
                        "surname = '%s', " +
                        "birth_date = '%s', " +
                        "death_date = '%s', " +
                        "gender = '%s', " +
                        "birth_place = '%s', " +
                        "death_place = '%s', " +
                        "address = '%s', " +
                        "biography_url = '%s', " +
                        "picture_url = '%s' " +
                        "where id = %d;",
                table,
                name, surname, birthDate.toString(), deathDate, gender, birthPlace, deathPlace, address, biographyUrl, pictureUrl,
                primaryKey);
    }

    public static String queryFind(String condition) {
        return "select m.id, m.name, m.surname, m.birth_date, m.death_date, m.gender, m.birth_place, m.death_place, m.address, m.biography_url, m.picture_url" +
                " from member m" +
                " where m." + condition + ";";
    }

    public static String deleteQuery(Long id) {
        return String.format("delete from %s where id = %d;", table, id);
    }

    public static String queryFindAll() {
        return "select m.id, m.name, m.surname, m.birth_date, m.death_date, m.gender, m.birth_place, m.death_place, m.address, m.biography_url, m.picture_url" +
                " from member m;";
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDate getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(LocalDate deathDate) {
        this.deathDate = deathDate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getDeathPlace() {
        return deathPlace;
    }

    public void setDeathPlace(String deathPlace) {
        this.deathPlace = deathPlace;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBiographyUrl() {
        return biographyUrl;
    }

    public void setBiographyUrl(String biographyUrl) {
        this.biographyUrl = biographyUrl;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
