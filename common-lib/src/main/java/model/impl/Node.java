package model.impl;

import model.Entity;
import model.enums.Gender;

import java.sql.*;
import java.time.LocalDate;
import java.util.*;

public class Node implements Entity {

    private static final String insertAttributes = "ancestry, member_id, tree_id, user_id";
    private static final String table = "node";

    private Long id;
    private final FamilyTree familyTree;
    private String ancestry;
    private Member member;
    private Map<Long, Node> children;
    private final User user;

    public Node(Long id, FamilyTree familyTree, String ancestry, Member member, User user) {
        this.id = id;
        this.familyTree = familyTree;
        this.ancestry = ancestry;
        this.member = member;
        this.children = new HashMap<>();
        this.user = user;
    }

    public Node(FamilyTree familyTree, String ancestry, Member member, User user) {
        this.familyTree = familyTree;
        this.ancestry = ancestry;
        this.member = member;
        this.user = user;
        this.children = new HashMap<>();
    }

    @Override
    public String queryInsert()  {
        return String.format("insert into %s (%s) values ('%s', %d, %d, %d);", table, insertAttributes,
                ancestry, member.getId(), familyTree.getId(), user.getId());
    }

    @Override
    public String queryUpdate(long primaryKey) {
        return String.format("update %s set " +
                        "ancestry = '%s', " +
                        "member_id = %d, " +
                        "tree_id = %d, " +
                        "user_id = %d " +
                        "where id = %d;",
                table, ancestry, member.getId(), familyTree.getId(), user.getId(), primaryKey);
    }

    public static String deleteQuery(Long id){
        return String.format("delete from %s where id = %d;", table, id);
    }

    public static String getDBTableName() {
        return "node";
    }

    public static String getInsertAttributes() {
        return "ancestry, member_id, tree_id, user_id";
    }

    public static String getAllAttributes() {
        return "id, " + getInsertAttributes();
    }

    public static String queryFind(String condition) {
        return "select n.id, n.ancestry, n.member_id, n.tree_id, n.user_id," +
                " t.name, t.description, t.date_from, t.user_id," +
                " m.name, m.surname, m.birth_date, m.death_date, m.gender, m.birth_place, m.death_place, m.address, m.biography_url, m.picture_url," +
                " u.username, u.password, u.email, u.name, u.surname," +
                " tu.username, tu.password, tu.email, tu.name, tu.surname" +
                " from node n join tree t on (n.tree_id = t.id) " +
                " join member m on (n.member_id = m.id) " +
                " join user u on (n.user_id = u.id)" +
                " join user tu on (tu.id = t.user_id)" +
                " where n." + condition + ";";
    }

    public static String queryFindAll() {
        return "select n.id, n.ancestry, n.member_id, n.tree_id, n.user_id," +
                " t.name, t.description, t.date_from, t.user_id," +
                " m.name, m.surname, m.birth_date, m.death_date, m.gender, m.birth_place, m.death_place, m.address, m.biography_url, m.picture_url," +
                " u.username, u.password, u.email, u.name, u.surname," +
                " tu.username, tu.password, tu.email, tu.name, tu.surname" +
                " from node n join tree t on (n.tree_id = t.id) " +
                " join member m on (n.member_id = m.id) " +
                " join user u on (n.user_id = u.id)" +
                " join user tu on (tu.id = t.user_id);";
    }

    public static Node toEntity(ResultSet rs) throws SQLException {
        /*podaci o cvoru*/
        long nodeID = rs.getLong("n.id");
        String ancestry = rs.getString("n.ancestry");
        /*podaci o stablu*/
        long treeID = rs.getLong("n.tree_id");
        String treeName = rs.getString("t.name");
        String treeDescription = rs.getString("t.description");
        LocalDate treeDateFrom = rs.getDate("t.date_from").toLocalDate();
        /*podaci o kreatoru cvora*/
        long userID = rs.getLong("n.user_id");
        String u_username = rs.getString("u.username");
        String u_password = rs.getString("u.password");
        String u_email = rs.getString("u.email");
        String u_name = rs.getString("u.name");
        String u_surname = rs.getString("u.surname");
        /*podaci o kreatoru stabla*/
        long treeUserId = rs.getLong("t.user_id");
        String tu_username = rs.getString("tu.username");
        String tu_password = rs.getString("tu.password");
        String tu_email = rs.getString("tu.email");
        String tu_name = rs.getString("tu.name");
        String tu_surname = rs.getString("tu.surname");
        /*podaci o clanu*/
        long memberID = rs.getLong("n.member_id");
        String name = rs.getString("m.name");
        String surname = rs.getString("m.surname");
        LocalDate birthDate = rs.getDate("m.birth_date").toLocalDate();
        java.sql.Date death = rs.getDate("m.death_date");
        LocalDate deathDate = death == null ? null : death.toLocalDate();
        Gender gender = Gender.valueOf(rs.getString("m.gender"));
        String birthPlace = rs.getString("m.birth_place");
        String deathPlace = rs.getString("m.death_place");
        String address = rs.getString("m.address");
        String biographyUrl = rs.getString("m.biography_url");
        String pictureUrl = rs.getString("m.picture_url");

        User treeMaker = new User(treeUserId, tu_username, tu_password, tu_email, tu_name, tu_surname);
        FamilyTree familyTree = new FamilyTree(treeID, treeName, treeDescription, treeDateFrom, treeMaker);
        User user = new User(userID, u_username, u_password, u_email, u_name, u_surname);
        Member member = new Member(memberID, name, surname, birthDate, deathDate, gender,
                birthPlace, deathPlace, address, biographyUrl, pictureUrl);

        return new Node(nodeID, familyTree, ancestry, member, user);
    }

    public boolean hasParent() {
        return ancestry.split("/").length > 0;
    }


    public User getUser() {
        return user;
    }

    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public FamilyTree getFamilyTree() {
        return familyTree;
    }

    public String getAncestry() {
        return ancestry;
    }

    public void setAncestry(String ancestry) {
        this.ancestry = ancestry;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Map<Long, Node> getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return member.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node node = (Node) o;
        return Objects.equals(getId(), node.getId());
    }


}
