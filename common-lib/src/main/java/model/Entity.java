package model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@JsonSerialize
public interface Entity extends Serializable {
    void setId(long id);
    long getId();
    String queryInsert() ;
    String queryUpdate(long primaryKey);

}
