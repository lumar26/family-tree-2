package model.enums;

public enum Role {
    PARENT("Roditelj"), CHILD("Dete");

    private final String value;

    Role(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static Role getByValue(String value) {
        if (value.equals("Roditelj")) return PARENT;
        if (value.equals("Dete")) return CHILD;
        return null;
    }

    public String getValue() {
        return value;
    }
}
