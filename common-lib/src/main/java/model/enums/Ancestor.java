package model.enums;

public enum Ancestor {
    FIRST("Otac/Majka"),
    SECOND("Deda/Baba"),
    THIRD("Preadeda/Prababa"),
    FOURTH("Čukundeda/Čukunbaba"),
    FIFTH("Navrdeda/Navrbaba"),
    SIXTH("Kurđel/Kurđela"),
    SEVENTH("Askurđel/Askurđela"),
    EIGHTH("Kurđup/Kurđupa"),
    NINTH("Kurlebalo/Kurlebala"),
    TENTH("Surdukov/Surdukova"),
    ELEVENTH("Surdepač/Surdepača"),
    TWELFTH("Parđupam/Parđupana"),
    THIRTEENTH("Ožmikur/Ožmikura"),
    FOURTEENTH("Kurajber/Kurajbera"),
    FIFTEENTH("Sajkatav/Sajkatava"),
    SIXTEENTH("Beli orao/Bela pčela"),
    ;
    private final String value;

    Ancestor(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getFemale(){
        return value.split("/")[1];
    }

    public String getMale(){
        return value.split("/")[0];
    }
}
