package model.enums;

public enum KinshipType {
    LATERAL, VERTICAL
}
