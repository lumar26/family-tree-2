package exception.enums;

public enum BoundaryType {
    RESTRICTED, CASCADES
}
