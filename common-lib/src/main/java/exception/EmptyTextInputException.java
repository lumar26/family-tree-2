package exception;

public class EmptyTextInputException extends Exception {
    public EmptyTextInputException(String message) {
        super(message);
    }
}
