package prop;

import java.io.*;
import java.util.Properties;
import java.util.Set;

public class TransferProperties {
    private static TransferProperties instance;

    private final Properties props;

    private TransferProperties() {
        this.props = new Properties();
        try {
            // neophodno je korisiti Reader - Writer kombinaciju ili InputStream - OutputStream kombinaciju, nikako mešovito
//            Reader reader = new FileReader("transfer.properties");
            InputStream reader = getClass().getClassLoader().getResourceAsStream("transfer.properties");
            props.load(reader);
//            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Could not load properties");
        }
    }

    public static TransferProperties getInstance() {
        if (instance == null) instance = new TransferProperties();
        return instance;
    }

    public String getProperty(String key) {
        return props.getProperty(key);
    }

    public Set<String> getAllPropertyNames() {
        return props.stringPropertyNames();
    }

    public boolean containsKey(String key) {
        return props.containsKey(key);
    }

    public void setProperty(String key, String value) throws IOException {
//        FileWriter out = new FileWriter("transfer.properties");
        OutputStream out = new FileOutputStream("transfer.properties");
        props.setProperty(key, value);
        props.store(out, null);
        out.close();
    }
}
