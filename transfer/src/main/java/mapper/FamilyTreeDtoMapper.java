package mapper;

import dto.impl.FamilyTreeDTO;
import model.impl.FamilyTree;

public final class FamilyTreeDtoMapper {
    private FamilyTreeDtoMapper(){}

    public static FamilyTreeDTO toFamilyTreeDto(FamilyTree entity) {
        return new FamilyTreeDTO(entity.getId(), entity.toString());
    }

    public static FamilyTreeDTO[] toFamilyTreeDtoList(FamilyTree[] trees){
        FamilyTreeDTO[] dtos = new FamilyTreeDTO[trees.length];
        for (int i = 0; i < dtos.length; i++) {
            dtos[i] = toFamilyTreeDto(trees[i]);
        }
        return dtos;
    }
}
