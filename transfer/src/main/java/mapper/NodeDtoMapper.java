package mapper;

import dto.impl.MemberInfoDTO;
import dto.impl.NodeDTO;
import dto.impl.NodeUpdateDTO;
import dto.impl.TreeNodeDTO;
import model.impl.Member;
import model.impl.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class NodeDtoMapper {
    private NodeDtoMapper() {}

    public static NodeDTO toNodeDto(Node node){
        return new NodeDTO(node.getId(), node.getMember().getGender(), node.toString());
    }

    public static NodeDTO[] toNodeDtoList(Node[] nodes){
        NodeDTO[] dtos = new NodeDTO[nodes.length];
        for (int i = 0; i < dtos.length; i++) {
            dtos[i] = toNodeDto(nodes[i]);
        }
        return dtos;
    }

    public static TreeNodeDTO toTreeNodeDto(Node node) {
        Map<Long, Node> children = node.getChildren();
        List<TreeNodeDTO> dtoChildren = new ArrayList<>();
        for (Node child : children.values()){
            dtoChildren.add(NodeDtoMapper.toTreeNodeDto(child));
        }
        return new TreeNodeDTO(node.getId(), node.toString(), dtoChildren);
    }

    public static NodeUpdateDTO toNodeUpdateDto(Node node) {
        Member member = node.getMember();
        MemberInfoDTO memberData = MemberDtoMapper.toMemberInfoDto(member);
        return new NodeUpdateDTO(node.getId(), memberData, 0, node.getFamilyTree().getId(), node.getUser().getId(), null);
    }

    public static NodeUpdateDTO[] toNodeUpdateDtoList(Node[] nodesOfUSer) {
        NodeUpdateDTO[] dtos = new NodeUpdateDTO[nodesOfUSer.length];
        for (int i = 0; i < dtos.length; i++) {
            dtos[i] = toNodeUpdateDto(nodesOfUSer[i]);
        }
        return dtos;
    }
}
