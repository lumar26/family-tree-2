package mapper;

import dto.impl.MemberInfoDTO;
import exception.EmptyTextInputException;
import model.enums.Gender;
import model.impl.Member;

import java.time.LocalDate;
import java.util.regex.Pattern;

public final class MemberDtoMapper {
    private MemberDtoMapper(){}

    public static Member toEntity(MemberInfoDTO memberInfo){
        return new Member(memberInfo.getId(), memberInfo.getName(), memberInfo.getSurname(), memberInfo.getBirthDate()
                , memberInfo.getDeathDate(), memberInfo.getGender(), memberInfo.getBirthPlace(), memberInfo.getDeathPlace()
                , memberInfo.getAddress(), memberInfo.getBiographyUrl(), memberInfo.getPictureUrl());
    }

    public static MemberInfoDTO toMemberInfoDto(Member m) {
        return new MemberInfoDTO(m.getId(), m.getName(), m.getSurname(), m.getBirthDate(), m.getDeathDate(), m.getGender(),
                m.getBirthPlace(), m.getDeathPlace(), m.getAddress(), m.getBiographyUrl(), m.getPictureUrl());
    }

    public static MemberInfoDTO factory(String name, String surname, Gender gender,
                                        LocalDate birthDate, LocalDate deathDate, String address,
                                        String birthPlace, String deathPlace, String biographyURL, String pictureURL) throws EmptyTextInputException {
        if (name == null || name.isBlank() || name.isEmpty() ||
                surname == null || surname.isEmpty() || surname.isBlank())
            throw new EmptyTextInputException("Član mora imati defifsano ime i prezime.");

        // regex za ime i prezime, poboljšanje performansi u odnosu na 2 "matches" metode
        Pattern pattern = Pattern.compile("^[a-zA-Z\u0100-\u017F\u2DE0-\u2DFF ]*$");

        if (!pattern.matcher(name).matches() || !pattern.matcher(name).matches())
            throw new EmptyTextInputException("Ime i prezime člana moraju sadržati samo latinična slova i/ili blanko znake");

        return new MemberInfoDTO(-1, name, surname, birthDate, deathDate,
                gender, birthPlace, deathPlace, address,
                biographyURL, pictureURL);
    }
}
