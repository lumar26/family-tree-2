package mapper;

import dto.impl.UserDTO;
import model.impl.User;

public final class UserDtoMapper {

    private UserDtoMapper() {
    }

    public static UserDTO fromEntity(User user){
        return new UserDTO(user.getId(), user.getUsername(), user.getEmail());
    }

}
