package dto.impl;

import dto.DTO;
import model.impl.FamilyTree;
import model.impl.User;

import java.time.LocalDate;

public class FamilyTreeDataDTO implements DTO {
    private long id;
    private final String title;
    private final String description;
    private final LocalDate startDate;
    private final long userID;

    public FamilyTreeDataDTO(String title, String description, LocalDate startDate, long userID) {
        this.title = title;
        this.description = description;
        this.startDate = startDate;
        this.userID = userID;
    }

    public FamilyTree toEntity(){
        User user = new User();
        user.setId(userID);
        return new FamilyTree(title, description, startDate, user);
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public long getUserID() {
        return userID;
    }
}
