package dto.impl;

import dto.DTO;

public class NodeSaveDTO implements DTO {
    private final long treeID;
    private final String ancestry;
    private final long userID;
    private final long spouseID;
    private final FamilyMemberDTO familyMember;
    private final MemberInfoDTO memberData;

    public NodeSaveDTO(long treeID, String ancestry, long userID, long spouseID, FamilyMemberDTO familyMember, MemberInfoDTO memberData) {
        this.treeID = treeID;
        this.ancestry = ancestry;
        this.userID = userID;
        this.spouseID = spouseID;
        this.familyMember = familyMember == null ? new FamilyMemberDTO(0L, "") : familyMember;
        this.memberData = memberData;
    }

    public long getTreeID() {
        return treeID;
    }

    public String getAncestry() {
        return ancestry;
    }

    public long getUserID() {
        return userID;
    }

    public long getSpouseID() {
        return spouseID;
    }

    public FamilyMemberDTO getFamilyMember() {
        return familyMember;
    }

    public MemberInfoDTO getMemberData() {
        return memberData;
    }
}
