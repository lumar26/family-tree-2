package dto.impl;

import dto.DTO;

public class FamilyTreeDTO implements DTO {
    private final long treeID;
    private final String displayText;

    public FamilyTreeDTO(long treeID, String text) {
        this.treeID = treeID;
        this.displayText = text;
    }

    public long getTreeID() {
        return treeID;
    }

    public String getDisplayText() {
        return displayText;
    }

    @Override
    public String toString() {
        return displayText;
    }
}
