package dto.impl;

import dto.DTO;
import model.impl.Member;
import model.impl.Node;

public class NodeUpdateDTO implements DTO {
    private final long nodeId;
    private final MemberInfoDTO memberData;
    private long spouseID;
    private final long treeID;
    private final long userID;
    private String spouseInfo;

    public NodeUpdateDTO(long nodeId, MemberInfoDTO memberData, long spouseID, long treeID, long userID, String spouseInfo) {
        this.nodeId = nodeId;
        this.memberData = memberData;
        this.spouseID = spouseID;
        this.treeID = treeID;
        this.userID = userID;
        this.spouseInfo = spouseInfo;
    }

    @Override
    public String toString() {
        return memberData.toString();
    }

    public long getNodeId() {
        return nodeId;
    }

    public MemberInfoDTO getMemberData() {
        return memberData;
    }

    public long getSpouseID() {
        return spouseID;
    }

    public long getTreeID() {
        return treeID;
    }

    public long getUserID() {
        return userID;
    }

    public void setSpouseID(long spouseID) {
        this.spouseID = spouseID;
    }

    public void setSpouseInfo(String spouseInfo) {
        this.spouseInfo = spouseInfo;
    }

    public String getSpouseInfo() {
        return spouseInfo;
    }

}
