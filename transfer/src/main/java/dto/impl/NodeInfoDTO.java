package dto.impl;

import dto.DTO;

public class NodeInfoDTO implements DTO {
    private final MemberInfoDTO memberInfo;
    private final NodeDTO spouse;
    private final FamilyTreeDTO spouseFamilyTree;

    public NodeInfoDTO(MemberInfoDTO memberInfo, NodeDTO spouse, FamilyTreeDTO spouseFamilyTree) {
        this.memberInfo = memberInfo;
        this.spouse = spouse;
        this.spouseFamilyTree = spouseFamilyTree;
    }

    public MemberInfoDTO getMemberInfo() {
        return memberInfo;
    }

    public NodeDTO getSpouse() {
        return spouse;
    }

    public FamilyTreeDTO getSpouseFamilyTree() {
        return spouseFamilyTree;
    }
}
