package dto.impl;

import dto.DTO;
import model.enums.Role;

public class FamilyMemberDTO implements DTO {
    private final long id;
    private final Role role;

    public FamilyMemberDTO(long id, String roleString) {
        this.id = id;
        this.role = Role.getByValue(roleString);
    }

    public long getId() {
        return id;
    }

    public Role getRole() {
        return role;
    }
}
