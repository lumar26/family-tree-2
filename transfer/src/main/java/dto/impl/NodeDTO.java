package dto.impl;

import dto.DTO;
import model.enums.Gender;

public class NodeDTO implements DTO {
    private final long nodeID;
    private final Gender gender;
    private final String displayText;

    public NodeDTO(long nodeID, Gender gender, String displayText) {
        this.nodeID = nodeID;
        this.gender = gender;
        this.displayText = displayText;
    }

    public long getNodeID() {
        return nodeID;
    }

    public String getDisplayText() {
        return displayText;
    }

    public Gender getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return displayText;
    }

}
