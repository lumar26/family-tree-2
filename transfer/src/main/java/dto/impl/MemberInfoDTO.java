package dto.impl;

import dto.DTO;
import model.enums.Gender;

import java.time.LocalDate;

public class MemberInfoDTO implements DTO {
    private long id;
    private final String name;
    private final String surname;
    private final LocalDate birthDate;
    private final LocalDate deathDate;
    private final Gender gender;
    private final String birthPlace;
    private final String deathPlace;
    private final String address;
    private final String biographyUrl;
    private final String pictureUrl;

    public MemberInfoDTO(long id, String name, String surname, LocalDate birthDate
            , LocalDate deathDate, Gender gender, String birthPlace, String deathPlace
            , String address, String biographyUrl, String pictureUrl) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
        this.gender = gender;
        this.birthPlace = birthPlace;
        this.deathPlace = deathPlace;
        this.address = address;
        this.biographyUrl = biographyUrl;
        this.pictureUrl = pictureUrl;
    }

    public boolean isAlive() {
        return deathDate == null;
    }

    @Override
    public String toString() {
        return name + " " + surname;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public LocalDate getDeathDate() {
        return deathDate;
    }

    public Gender getGender() {
        return gender;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public String getDeathPlace() {
        return deathPlace;
    }

    public String getAddress() {
        return address;
    }

    public String getBiographyUrl() {
        return biographyUrl;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }
}
