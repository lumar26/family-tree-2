package dto.impl;

import dto.DTO;
import mapper.NodeDtoMapper;
import model.impl.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TreeNodeDTO implements DTO {
    private final long nodeId;
    private final String currentNode;
    private final List<TreeNodeDTO> children;

    public TreeNodeDTO(long nodeId, String description, List<TreeNodeDTO> children) {
        this.nodeId = nodeId;
        this.currentNode = description;
        this.children = children;
    }

    @Override
    public String toString() {
        return currentNode;
    }

    public long getNodeId() {
        return nodeId;
    }

    public List<TreeNodeDTO> getChildren() {
        return children;
    }

    public TreeNodeDTO getChild(int i){
        return children.get(i);
    }

    public int getChildCount(){
        return children.size();
    }

    public boolean hasChildren(){
        return children.size() > 0;
    }

    public int getIndexOfChild(TreeNodeDTO child){
        return children.indexOf(child);
    }

}
