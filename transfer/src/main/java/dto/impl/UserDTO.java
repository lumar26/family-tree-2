package dto.impl;

import dto.DTO;
import model.impl.User;

public class UserDTO implements DTO {
    private final long userId;
    private final String username;
    private final String email;

    public UserDTO(long userId, String username, String email) {
        this.userId = userId;
        this.username = username;
        this.email = email;
    }

    public long getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return username;
    }
}
