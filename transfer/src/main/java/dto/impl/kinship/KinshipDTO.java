package dto.impl.kinship;

import dto.DTO;
import model.enums.KinshipType;

public abstract class KinshipDTO implements DTO {
    private final KinshipType kinshipType;
    private final long degree;

    public KinshipDTO(KinshipType kinshipType, int degree) {
        this.kinshipType = kinshipType;
        this.degree = degree;
    }

    public KinshipType getKinshipType() {
        return kinshipType;
    }

    public long getDegree() {
        return degree;
    }
}
