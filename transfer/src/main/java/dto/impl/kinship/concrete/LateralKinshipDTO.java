package dto.impl.kinship.concrete;

import dto.impl.NodeDTO;
import dto.impl.kinship.KinshipDTO;
import model.enums.KinshipType;

public class LateralKinshipDTO extends KinshipDTO {
    private boolean cousins;
    private NodeDTO member1;
    private NodeDTO member2;
    private String member1Role;
    private String member2Role;

    public LateralKinshipDTO(KinshipType kinshipType, int degree, boolean cousins) {
        super(kinshipType, degree);
        this.cousins = cousins;
    }

    public boolean areCousins() {
        return cousins;
    }
}
