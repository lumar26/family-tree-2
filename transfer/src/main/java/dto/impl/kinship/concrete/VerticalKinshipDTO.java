package dto.impl.kinship.concrete;

import dto.impl.NodeDTO;
import dto.impl.kinship.KinshipDTO;
import model.enums.Ancestor;
import model.enums.Gender;
import model.enums.KinshipType;

public class VerticalKinshipDTO extends KinshipDTO {

    private NodeDTO ancestor;
    private String ancestorRole;
    private NodeDTO descendant;

    public VerticalKinshipDTO(KinshipType kinshipType, int degree, NodeDTO ancestor, NodeDTO descendant) {
        super(kinshipType, degree);
        this.ancestor = ancestor;
        this.ancestorRole = determineRole(degree, ancestor);
        this.descendant = descendant;
    }

    private String determineRole(int degree, NodeDTO ancestor) {
        Ancestor[] ancestors = Ancestor.values();

        if (ancestor.getGender().equals(Gender.MALE)) return ancestors[degree - 1].getMale();
        return ancestors[degree - 1].getFemale();
    }

    public VerticalKinshipDTO(KinshipType kinshipType, int degree) {
        super(kinshipType, degree);
    }

    public NodeDTO getAncestor() {
        return ancestor;
    }

    public String getAncestorRole() {
        return ancestorRole;
    }

    public NodeDTO getDescendant() {
        return descendant;
    }
}
